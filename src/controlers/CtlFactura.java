package controlers;

import java.util.LinkedList;

import classes.ClsFactura;
import models.ModelFactura;

public class CtlFactura {
    private ModelFactura modelFactura;

    public CtlFactura() {
        this.modelFactura = new ModelFactura();
    }
    public ClsFactura Create(ClsFactura factura){
        return this.modelFactura.Create(factura);
    }
    public ClsFactura Edit(ClsFactura factura){
        return this.modelFactura.Edit(factura);
    }
    public boolean Delete(ClsFactura factura){
        return this.modelFactura.Delete(factura);
    }
    public ClsFactura Search(Integer id){
        return this.modelFactura.Search(id);
    }
    public LinkedList<ClsFactura> All(){
        return this.modelFactura.All();
    }
}
