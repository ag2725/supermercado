/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlers;

import classes.ClsCategoria;
import java.util.LinkedList;
import models.ModelCategoria;

/**
 *
 * @author ever
 */
public class CtlCategoria {
    private ModelCategoria modelCategoria;

    public CtlCategoria() {
        this.modelCategoria = new ModelCategoria();
    }
    public ClsCategoria Create(ClsCategoria categoria){
        return this.modelCategoria.Create(categoria);
    }
    public ClsCategoria Edit(ClsCategoria categoria){
        return this.modelCategoria.Edit(categoria);
    }
    public boolean Delete(ClsCategoria categoria){
        return this.modelCategoria.Delete(categoria);
    }
    public ClsCategoria Search(Integer id){
        return this.modelCategoria.Search(id);
    }
    public LinkedList<ClsCategoria> All(){
        return this.modelCategoria.All();
    }
}
