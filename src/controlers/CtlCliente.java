/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlers;

import classes.ClsClientes;
import java.util.LinkedList;
import models.ModelClientes;

/**
 *
 * @author ever
 */
public class CtlCliente {
    private ModelClientes modelClientes;

    public CtlCliente() {
        this.modelClientes = new ModelClientes();
    }
    public ClsClientes Create(ClsClientes cliente){
        return this.modelClientes.Create(cliente);
    }
    public ClsClientes Edit(ClsClientes cliente){
        return this.modelClientes.Edit(cliente);
    }
    public boolean Delete(ClsClientes cliente){
        return this.modelClientes.Delete(cliente);
    }
    public ClsClientes Search(Integer id){
        return this.modelClientes.Search(id);
    }
    public LinkedList<ClsClientes> All(){
        return this.modelClientes.All();
    }
}
