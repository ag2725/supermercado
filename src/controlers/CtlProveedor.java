/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlers;

import classes.ClsProveedor;
import java.util.LinkedList;
import models.ModelProveedor;

/**
 *
 * @author ever
 */
public class CtlProveedor {
    private ModelProveedor modelProveedor;

    public CtlProveedor() {
        this.modelProveedor = new ModelProveedor();
    }
    public ClsProveedor Create(ClsProveedor proveedor){
        return this.modelProveedor.Create(proveedor);
    }
    public ClsProveedor Edit(ClsProveedor proveedor){
        return this.modelProveedor.Edit(proveedor);
    }
    public boolean Delete(ClsProveedor proveedor){
        return this.modelProveedor.Delete(proveedor);
    }
    public ClsProveedor Search(Integer id){
        return this.modelProveedor.Search(id);
    }
    public LinkedList<ClsProveedor> All(){
        return this.modelProveedor.All();
    }
}
