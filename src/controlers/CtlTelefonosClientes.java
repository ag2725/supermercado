/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlers;

import classes.ClsTelefonosClientes;
import models.ModelTelefonosClientes;

/**
 *
 * @author ever
 */
public class CtlTelefonosClientes {
    private ModelTelefonosClientes modelTelefono;

    public CtlTelefonosClientes() {
        this.modelTelefono = new ModelTelefonosClientes();
    }
    public boolean Create(ClsTelefonosClientes telefono){
        return this.modelTelefono.Create(telefono);
    }
    public boolean Edit(ClsTelefonosClientes telefono){
        return this.modelTelefono.Edit(telefono);
    }
    public boolean Delete(ClsTelefonosClientes telefono){
        return this.modelTelefono.Delete(telefono);
    }
}
