package controlers;

import java.util.LinkedList;

import classes.ClsProductos;
import models.ModelProducto;

public class CtlProducto {
  private ModelProducto modelProducto;

  public CtlProducto() {
      this.modelProducto = new ModelProducto();
  }
  public ClsProductos Create(ClsProductos producto){
      return this.modelProducto.Create(producto);
  }
  public ClsProductos Edit(ClsProductos producto){
      return this.modelProducto.Edit(producto);
  }
  public boolean Delete(ClsProductos producto){
      return this.modelProducto.Delete(producto);
  }
  public ClsProductos Search(Integer id){
      return this.modelProducto.Search(id);
  }
  public LinkedList<ClsProductos> All(){
      return this.modelProducto.All();
  }
}
