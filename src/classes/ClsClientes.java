/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.LinkedList;

/**
 *
 * @author ever
 */
public class ClsClientes {
    private Integer Id;
    private String CC, Nombres, Apellidos,Direccion,CorreoElectronico;
    private LinkedList<ClsTelefonosClientes> telefonosClientes = new LinkedList<>();

    public ClsClientes(String CC, String Nombres, String Apellidos, String Direccion, String CorreoElectronico) {
        this.CC = CC;
        this.Nombres = Nombres;
        this.Apellidos = Apellidos;
        this.Direccion = Direccion;
        this.CorreoElectronico = CorreoElectronico;
    }

    public ClsClientes(Integer Id, String CC, String Nombres, String Apellidos, String Direccion, String CorreoElectronico) {
        this.Id = Id;
        this.CC = CC;
        this.Nombres = Nombres;
        this.Apellidos = Apellidos;
        this.Direccion = Direccion;
        this.CorreoElectronico = CorreoElectronico;
    }
    
    @Override
    public String toString(){
        return this.Nombres+" "+this.Apellidos;
    };

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getCC() {
        return CC;
    }

    public void setCC(String CC) {
        this.CC = CC;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public String getCorreoElectronico() {
        return CorreoElectronico;
    }

    public void setCorreoElectronico(String CorreoElectronico) {
        this.CorreoElectronico = CorreoElectronico;
    }

    public LinkedList<ClsTelefonosClientes> getTelefonosClientes() {
        return telefonosClientes;
    }

    public void setTelefonosClientes(LinkedList<ClsTelefonosClientes> telefonosClientes) {
        this.telefonosClientes = telefonosClientes;
    }
    
    public void setTelefono(ClsTelefonosClientes telefono){
        this.telefonosClientes.add(telefono);
    }
}
