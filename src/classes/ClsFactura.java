/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;
import java.util.Date;
import java.util.LinkedList;
/**
 *
 * @author ever
 */
public class ClsFactura {
    private Integer Id;
    private Date Fecha;
    private Integer idClient;
    private Integer Descuento;
    private Float Total;
    private String Cliente,CC;
    
    private LinkedList<ClsProductoFactura> productos = new LinkedList<>();

    public ClsFactura() {
    }

    public ClsFactura(Integer id, Date fecha, Integer idClient, Integer descuento, Float total, String cliente,
            String cC) {
        Id = id;
        Fecha = fecha;
        this.idClient = idClient;
        Descuento = descuento;
        Total = total;
        Cliente = cliente;
        CC = cC;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date fecha) {
        Fecha = fecha;
    }

    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    public Integer getDescuento() {
        return Descuento;
    }

    public void setDescuento(Integer descuento) {
        Descuento = descuento;
    }

    public Float getTotal() {
        return Total;
    }

    public void setTotal(Float total) {
        Total = total;
    }

    public LinkedList<ClsProductoFactura> getProductos() {
        return productos;
    }

    public void setProducto(ClsProductoFactura producto) {
        this.productos.add(producto);
    }

    public void removeProducto(ClsProductoFactura index) {
        this.productos.remove(index);
    }

    public String getCliente() {
        return Cliente;
    }

    public void setCliente(String cliente) {
        Cliente = cliente;
    }

    public String getCC() {
        return CC;
    }

    public void setCC(String cC) {
        CC = cC;
    }
}
