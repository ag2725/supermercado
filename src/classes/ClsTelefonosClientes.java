/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author ever
 */
public class ClsTelefonosClientes {
    private Integer Id, idCliente;
    private String telefono;

    public ClsTelefonosClientes(Integer idCliente, String telefono) {
        this.idCliente = idCliente;
        this.telefono = telefono;
    }

    public ClsTelefonosClientes(Integer Id, Integer idCliente, String telefono) {
        this.Id = Id;
        this.idCliente = idCliente;
        this.telefono = telefono;
    }
    
    @Override
    public String toString(){
        return ""+this.telefono;
    };

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }
    
}
