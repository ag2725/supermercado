/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author ever
 */
public class ClsProveedor {
    private Integer Id;
    private String Nit;
    private String RazonSocial, Telefono, CorreoElectronico, RepresentanteLegal,SitioWeb;
    private Integer idDireccion;
    private String CalleCarrera, Numero, Barrio, Ciudad;

    public ClsProveedor(String Nit, String RazonSocial, String Telefono,
                        String CorreoElectronico, String RepresentanteLegal,
                        String SitioWeb, String CalleCarrera, String Numero,
                        String Barrio, String Ciudad) {
        this.Nit = Nit;
        this.RazonSocial = RazonSocial;
        this.Telefono = Telefono;
        this.CorreoElectronico = CorreoElectronico;
        this.RepresentanteLegal = RepresentanteLegal;
        this.SitioWeb = SitioWeb;
        this.CalleCarrera = CalleCarrera;
        this.Numero = Numero;
        this.Barrio = Barrio;
        this.Ciudad = Ciudad;
    }

    public ClsProveedor(Integer Id, String Nit, String RazonSocial, String Telefono,
                        String CorreoElectronico, String RepresentanteLegal,
                        String SitioWeb, Integer idDireccion, String CalleCarrera,
                        String Numero, String Barrio, String Ciudad) {
        this.Id = Id;
        this.Nit = Nit;
        this.RazonSocial = RazonSocial;
        this.Telefono = Telefono;
        this.CorreoElectronico = CorreoElectronico;
        this.RepresentanteLegal = RepresentanteLegal;
        this.SitioWeb = SitioWeb;
        this.idDireccion = idDireccion;
        this.CalleCarrera = CalleCarrera;
        this.Numero = Numero;
        this.Barrio = Barrio;
        this.Ciudad = Ciudad;
    }
    
    @Override
    public String toString(){
        return this.Id+" - "+this.RazonSocial;
    };

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNit() {
        return Nit;
    }

    public void setNit(String Nit) {
        this.Nit = Nit;
    }

    public String getRazonSocial() {
        return RazonSocial;
    }

    public void setRazonSocial(String RazonSocial) {
        this.RazonSocial = RazonSocial;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getCorreoElectronico() {
        return CorreoElectronico;
    }

    public void setCorreoElectronico(String CorreoElectronico) {
        this.CorreoElectronico = CorreoElectronico;
    }

    public String getRepresentanteLegal() {
        return RepresentanteLegal;
    }

    public void setRepresentanteLegal(String RepresentanteLegal) {
        this.RepresentanteLegal = RepresentanteLegal;
    }

    public String getSitioWeb() {
        return SitioWeb;
    }

    public void setSitioWeb(String SitioWeb) {
        this.SitioWeb = SitioWeb;
    }

    public String getCalleCarrera() {
        return CalleCarrera;
    }

    public void setCalleCarrera(String CalleCarrera) {
        this.CalleCarrera = CalleCarrera;
    }

    public String getNumero() {
        return Numero;
    }

    public void setNumero(String Numero) {
        this.Numero = Numero;
    }

    public String getBarrio() {
        return Barrio;
    }

    public void setBarrio(String Barrio) {
        this.Barrio = Barrio;
    }

    public String getCiudad() {
        return Ciudad;
    }

    public void setCiudad(String Ciudad) {
        this.Ciudad = Ciudad;
    }

    public Integer getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }
}
