/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author ever
 */
public class ClsProductos {
    private Integer Id;
    private String Nombre;
    private Float PrecioActual;
    private Integer Existencia;
    private Integer idCategoria, idProveedor;
    private String categoria, proveedor;
    private Integer vendidos;
    //Para Crear
    public ClsProductos(String Nombre, Float PrecioActual, Integer Existencia, Integer idCategoria, Integer idProveedor) {
        this.Nombre = Nombre;
        this.PrecioActual = PrecioActual;
        this.Existencia = Existencia;
        this.idCategoria = idCategoria;
        this.idProveedor = idProveedor;
    }
    //Para Editar
    public ClsProductos(Integer Id, String Nombre, Float PrecioActual, Integer Existencia, Integer idCategoria, Integer idProveedor) {
        this.Id = Id;
        this.Nombre = Nombre;
        this.PrecioActual = PrecioActual;
        this.Existencia = Existencia;
        this.idCategoria = idCategoria;
        this.idProveedor = idProveedor;
    }
    // Para optener
    public ClsProductos(Integer Id, String Nombre, Float PrecioActual, Integer Existencia, Integer idCategoria, Integer idProveedor, String categoria, String proveedor, Integer vendidos) {
        this.Id = Id;
        this.Nombre = Nombre;
        this.PrecioActual = PrecioActual;
        this.Existencia = Existencia;
        this.idCategoria = idCategoria;
        this.idProveedor = idProveedor;
        this.categoria = categoria;
        this.proveedor = proveedor;
        this.vendidos = vendidos;
    }
    
    @Override
    public String toString(){
        return this.Nombre+"("+this.Existencia+")";
    };

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public Float getPrecioActual() {
        return PrecioActual;
    }

    public void setPrecioActual(Float PrecioActual) {
        this.PrecioActual = PrecioActual;
    }

    public Integer getExistencia() {
        return Existencia;
    }

    public void setExistencia(Integer Existencia) {
        this.Existencia = Existencia;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public Integer getVendidos() {
        return vendidos;
    }

    public void setVendidos(Integer vendidos) {
        this.vendidos = vendidos;
    }
}
