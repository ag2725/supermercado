/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author ever
 */
public class ClsCategoria {
    private Integer Id;
    private String Nombre, Descripcion;
    private Integer productos;

    public ClsCategoria(String Nombre, String Descripcion) {
        this.Nombre = Nombre;
        this.Descripcion = Descripcion;
    }

    public ClsCategoria(Integer Id, String Nombre, String Descripcion, Integer productos) {
        this.Id = Id;
        this.Nombre = Nombre;
        this.Descripcion = Descripcion;
        this.productos = productos;
    }
    
    @Override
    public String toString(){
        return this.Id+": "+this.Nombre;
    };

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public Integer getProductos() {
        return productos;
    }

    public void setProductos(Integer productos) {
        this.productos = productos;
    }
}
