/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author ever
 */
public class ClsProductoFactura {
    private Integer Id;
    private Float PrecioUnitario;
    private Integer Cantidad,idFactura,idProducto;
    
    private String Nombre;
    private Integer Existencia;

    public ClsProductoFactura(Float PrecioUnitario, Integer Cantidad, Integer idFactura, Integer idProducto, String Nombre, Integer Existencia) {
        this.PrecioUnitario = PrecioUnitario;
        this.Cantidad = Cantidad;
        this.idFactura = idFactura;
        this.idProducto = idProducto;
        this.Nombre = Nombre;
        this.Existencia = Existencia;
    }

    public ClsProductoFactura(Integer Id, Float PrecioUnitario, Integer Cantidad, Integer idFactura, Integer idProducto, String Nombre, Integer Existencia) {
        this.Id = Id;
        this.PrecioUnitario = PrecioUnitario;
        this.Cantidad = Cantidad;
        this.idFactura = idFactura;
        this.idProducto = idProducto;
        this.Nombre = Nombre;
        this.Existencia = Existencia;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Float getPrecioUnitario() {
        return PrecioUnitario;
    }

    public void setPrecioUnitario(Float PrecioUnitario) {
        this.PrecioUnitario = PrecioUnitario;
    }

    public Integer getCantidad() {
        return Cantidad;
    }

    public void setCantidad(Integer Cantidad) {
        this.Cantidad = Cantidad;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public Integer getExistencia() {
        return Existencia;
    }

    public void setExistencia(Integer Existencia) {
        this.Existencia = Existencia;
    }
    
    
}
