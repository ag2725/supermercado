/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import classes.ClsCategoria;
import classes.ClsClientes;
import classes.ClsFactura;
import classes.ClsProductoFactura;
import classes.ClsProductos;
import classes.ClsProveedor;
import classes.ClsTelefonosClientes;
import controlers.CtlCategoria;
import controlers.CtlCliente;
import controlers.CtlFactura;
import controlers.CtlProducto;
import controlers.CtlProveedor;
import controlers.CtlTelefonosClientes;
import java.util.LinkedList;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ever
 */
public class FrmMain extends javax.swing.JFrame {
    CtlProveedor ctlProveedor = new CtlProveedor();
    CtlCategoria ctlCategoria = new CtlCategoria();
    CtlTelefonosClientes ctlTelefono = new CtlTelefonosClientes();
    CtlCliente ctlCliente = new CtlCliente();
    CtlProducto ctlProducto = new CtlProducto();
    CtlFactura ctlFactura = new CtlFactura();
    ClsClientes clienteActual = null;
    ClsFactura facturaActual = new ClsFactura();
    LinkedList<ClsCategoria> categoria_list;
    LinkedList<ClsProveedor> proveedor_list;
    LinkedList<ClsClientes> cliente_list;
    LinkedList<ClsProductos> productos_list;
    JComboBox<ClsProductos> cbSelectProducto = new JComboBox<>();
    /**
     * Creates new form FrmMain
     */
    public FrmMain() {
        initComponents();
        setLocationRelativeTo(null);
        setExtendedState(javax.swing.JFrame.MAXIMIZED_BOTH);
        setTitle("Supermercado");
        getProveedores();
        getCategorias();
        getClientes();
        getProductos();
        getFacturas();
        cbClienteFactura.setSelectedItem(null);
        cbCategoriaProducto.setSelectedItem(null);
        cbProveedorProducto.setSelectedItem(null);
        jlImagen.setIcon(new ImageIcon(getClass().getResource("/img/facturas.png")));

        pruebas();
    }
    public void buscarFactura(Integer id_factura){
        ClsFactura factura = ctlFactura.Search(id_factura);
        setFactura(factura);
    }
    public void guardarFactura(){
        try {
            ClsClientes clienteSeleccionado = (ClsClientes) cbClienteFactura.getSelectedItem();
            facturaActual.setCliente(clienteSeleccionado.getApellidos()+" "+clienteSeleccionado.getNombres());
            facturaActual.setCC(clienteSeleccionado.getCC());
            facturaActual.setIdClient(clienteSeleccionado.getId());
            facturaActual.setDescuento(
                Integer.parseInt(
                    txtDescuentoFactura.getText().equals("")?"0":txtDescuentoFactura.getText()
                )
            );
            if(facturaActual.getId()==null){
                setFactura(
                    ctlFactura.Create(facturaActual)
                );
            }else{
                setFactura(
                    ctlFactura.Edit(facturaActual)
                );
            }
            getProductos();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Verifique los campos Cliente y descuento!", "Error!", JOptionPane.ERROR_MESSAGE);
        }
        getFacturas();
//        try {
//            ClsClientes clienteFactura = (ClsClientes) cbClienteFactura.getSelectedItem();
//            Integer Id = null;
//            Integer Existencia = null;
//            if(!txtIdProducto.getText().equals("")){
//                Id = Integer.parseInt(txtIdProducto.getText());
//                Existencia = Integer.parseInt(txtExistenciaProducto.getText());
//            }
//    
//            String Nombre = txtNombreProducto.getText();
//            Float PrecioActual = Float.parseFloat(txtPrecioProducto.getText());
//            Integer idCategoria = categoria.getId();
//            Integer idProveedor = proveedor.getId();
//            if(Nombre.equals("")||idCategoria==null||idProveedor==null){
//                JOptionPane.showMessageDialog(
//                    this,
//                    "Completa los campos!",
//                    "Alerta",
//                    JOptionPane.WARNING_MESSAGE
//                );
//            }else{
//                if(Id==null){
//                    Existencia = 0;
//                    producto = new ClsProductos(Nombre, PrecioActual, Existencia, idCategoria, idProveedor);
//                    setProducto(ctlProducto.Create(producto));
//                    JOptionPane.showMessageDialog(
//                        this,
//                        "Producto creado!",
//                        "Alerta",
//                        JOptionPane.WARNING_MESSAGE
//                    );
//                }else{
//                    producto = new ClsProductos(Id, Nombre, PrecioActual, Existencia, idCategoria, idProveedor);
//                    setProducto(ctlProducto.Edit(producto));
//                    JOptionPane.showMessageDialog(
//                        this,
//                        "Producto editado!",
//                        "Alerta",
//                        JOptionPane.WARNING_MESSAGE
//                    );
//                }
//            }
//            getProductos();
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(
//                this, "Verifica los campos", 
//                "Error", JOptionPane.ERROR_MESSAGE
//            );
//        }
    }
    public void setFactura(ClsFactura factura){
        if(factura.getId()==null){
            btGuardarFactura.setEnabled(true);
            btAgregarProducto.setEnabled(true);
        }else{
            btGuardarFactura.setEnabled(false);
            btAgregarProducto.setEnabled(false);
        }
        facturaActual = factura;
        if(facturaActual.getDescuento()==null){
            try {
                facturaActual.setDescuento(
                    Integer.parseInt(txtDescuentoFactura.getText())
                );
            } catch (Exception e) {
                facturaActual.setDescuento(0);
            }  
        }else{
            txtDescuentoFactura.setText(""+facturaActual.getDescuento());
        }
        lbFactura.setText(""+ (factura.getId()!=null?factura.getId():"") );
        lbFecha.setText(""+ (factura.getFecha()!=null?factura.getFecha():"") );
        lbCC.setText(factura.getCC());
        lbCliente.setText(factura.getCliente());
        txtDescuentoFactura.setText(""+factura.getDescuento());
        for (int i = 0; i < cliente_list.size(); i++) {
            if (cliente_list.get(i).getId()==factura.getIdClient()) {
                cbClienteFactura.setSelectedIndex(i);
                break;
            }
        }
        DefaultTableModel model = (DefaultTableModel) tbFactura.getModel();
        while(model.getRowCount()>=1) {
            model.removeRow(0);
        }
        String[] list_data = new String[5];
        Float totalSinIva = 0f;
        for (ClsProductoFactura producto : factura.getProductos()) {
            list_data[0] = ""+producto.getIdProducto();
            list_data[1] = ""+producto.getNombre();
            list_data[2] = ""+producto.getCantidad();
            list_data[3] = ""+producto.getPrecioUnitario();
            list_data[4] = ""+(producto.getCantidad()*producto.getPrecioUnitario());
            model.addRow(list_data);
            totalSinIva += producto.getCantidad()*producto.getPrecioUnitario();
        }
        System.out.println(facturaActual.getId());
        if(facturaActual.getId()!=null&&totalSinIva<facturaActual.getTotal()){
            rbSi.setSelected(true);
//            rbNo.setSelected(false);
        }else if(facturaActual.getId()!=null){
//            rbSi.setSelected(false);
            rbNo.setSelected(true);
        }
        
        if(rbSi.isSelected()){
            lbIva.setText(""+(totalSinIva*0.19f));
            facturaActual.setTotal(totalSinIva*1.19f);
        }else{
            lbIva.setText("0");
            facturaActual.setTotal(totalSinIva);
        }
        lbDescuento.setText(""+facturaActual.getDescuento());
        lbTotal.setText(""+facturaActual.getTotal());
        tbFactura.setModel(model);
    }
    public void limpiarFactura(){
        setFactura(new ClsFactura());
        lbFactura.setText("");
        lbFecha.setText("");
        lbCC.setText("");
        lbCliente.setText("");
        txtDescuentoFactura.setText("0");
        cbClienteFactura.setSelectedItem(null);
        DefaultTableModel model = (DefaultTableModel) tbFactura.getModel();
        while(model.getRowCount()>=1) {
            model.removeRow(0);
        }
        tbFactura.setModel(model);
    }
    public void getFacturas(){
        LinkedList<ClsFactura> factura_list = ctlFactura.All();
        DefaultTableModel model = (DefaultTableModel) tbFacturas.getModel();
        while(model.getRowCount()>=1) {
            model.removeRow(0);
        }
        String[] list_data = new String[4];
        for (ClsFactura factura : factura_list) {
            list_data[0] = ""+factura.getId();
            list_data[1] = ""+factura.getCliente();
            list_data[2] = ""+factura.getCC();
            list_data[3] = ""+factura.getTotal();
            model.addRow(list_data);
        }
        tbFacturas.setModel(model);
    }
    
    public void buscarProducto(Integer id_cliente){
        ClsProductos producto = ctlProducto.Search(id_cliente);
        setProducto(producto);
    }
    public void guardarProducto(){
        try {
            ClsProductos producto;
            ClsCategoria categoria = (ClsCategoria)cbCategoriaProducto.getSelectedItem();
            ClsProveedor proveedor = (ClsProveedor)cbProveedorProducto.getSelectedItem();
            Integer Id = null;
            Integer Existencia = null;
            if(!txtIdProducto.getText().equals("")){
                Id = Integer.parseInt(txtIdProducto.getText());
                Existencia = Integer.parseInt(txtExistenciaProducto.getText());
            }
    
            String Nombre = txtNombreProducto.getText();
            Float PrecioActual = Float.parseFloat(txtPrecioProducto.getText());
            Integer idCategoria = categoria.getId();
            Integer idProveedor = proveedor.getId();
            if(Nombre.equals("")||idCategoria==null||idProveedor==null){
                JOptionPane.showMessageDialog(
                    this,
                    "Completa los campos!",
                    "Alerta",
                    JOptionPane.WARNING_MESSAGE
                );
            }else{
                if(Id==null){
                    Existencia = 0;
                    producto = new ClsProductos(Nombre, PrecioActual, Existencia, idCategoria, idProveedor);
                    setProducto(ctlProducto.Create(producto));
                    JOptionPane.showMessageDialog(
                        this,
                        "Producto creado!",
                        "Alerta",
                        JOptionPane.WARNING_MESSAGE
                    );
                }else{
                    producto = new ClsProductos(Id, Nombre, PrecioActual, Existencia, idCategoria, idProveedor);
                    setProducto(ctlProducto.Edit(producto));
                    JOptionPane.showMessageDialog(
                        this,
                        "Producto editado!",
                        "Alerta",
                        JOptionPane.WARNING_MESSAGE
                    );
                }
            }
            getProductos();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(
                this, "Verifica los campos", 
                "Error", JOptionPane.ERROR_MESSAGE
            );
        }
    }
    public void setProducto(ClsProductos producto){
        btEliminarProducto.setEnabled(true);
        btEditarProducto.setEnabled(true);
        btCrearProducto.setEnabled(false);
        txtIdProducto.setText(""+producto.getId());
        txtNombreProducto.setText(""+producto.getNombre());
        txtPrecioProducto.setText(""+producto.getPrecioActual());
        txtExistenciaProducto.setText(""+producto.getExistencia());
        for (int i = 0; i < categoria_list.size(); i++) {
            if (categoria_list.get(i).getId()==producto.getIdCategoria()) {
                cbCategoriaProducto.setSelectedIndex(i);
                break;
            }
        }
        for (int i = 0; i < proveedor_list.size(); i++) {
            if (proveedor_list.get(i).getId()==producto.getIdProveedor()) {
                cbProveedorProducto.setSelectedIndex(i);
                break;
            }
        }
//        // Los dos anteriores for se hicieron porque este metodo no funciona
//        cbProveedorProducto.setSelectedItem(
//                ctlProveedor.Search(producto.getIdProveedor())
//        );
    }
    public void limpiarProducto(){
        btEliminarProducto.setEnabled(false);
        btEditarProducto.setEnabled(false);
        btCrearProducto.setEnabled(true);
        txtIdProducto.setText("");
        txtNombreProducto.setText("");
        txtPrecioProducto.setText("");
        txtExistenciaProducto.setText("");
        cbCategoriaProducto.setSelectedItem(null);
        cbProveedorProducto.setSelectedItem(null);
    }
    public void getProductos(){
        productos_list = ctlProducto.All();
        DefaultTableModel model = (DefaultTableModel) tbProductos.getModel();
        while(model.getRowCount()>=1) {
            model.removeRow(0);
        }
        String[] list_data = new String[7];
        cbSelectProducto.removeAllItems();
        for (ClsProductos producto : productos_list) {
            cbSelectProducto.addItem(producto);
            list_data[0] = ""+producto.getId();
            list_data[1] = ""+producto.getNombre();
            list_data[2] = ""+producto.getPrecioActual();
            list_data[3] = ""+producto.getExistencia();
            list_data[4] = ""+producto.getVendidos();
            list_data[5] = ""+producto.getCategoria();
            list_data[6] = ""+producto.getProveedor();
            model.addRow(list_data);
        }
        tbProductos.setModel(model);
    }
    
    public void buscarCliente(Integer id_cliente){
        ClsClientes cliente = ctlCliente.Search(id_cliente);
        setCliente(cliente);
    }
    public void guardarCliente(){
        ClsClientes cliente;
        Integer Id = null;
        if(!txtIdCliente.getText().equals("")){
            Id = Integer.parseInt(txtIdCliente.getText());
        }
        String CC = txtCcCliente.getText();
        String Nombres = txtNombresCliente.getText();
        String Apellidos = txtApellidosCliente.getText();
        String Direccion = txtDireccionCliente.getText();
        String CorreoElectronico = txtCorreoCliente.getText();
        if(CC.equals("")||Nombres.equals("")||Apellidos.equals("")
        ||Direccion.equals("")||CorreoElectronico.equals("")){
            JOptionPane.showMessageDialog(
                this,
                "Completa los campos!",
                "Alerta",
                JOptionPane.WARNING_MESSAGE
            );
        }else{
            if(Id==null){
                cliente = new ClsClientes(CC, Nombres, Apellidos, Direccion, CorreoElectronico);
                setCliente(ctlCliente.Create(cliente));
                JOptionPane.showMessageDialog(
                    this,
                    "Cliente creado!",
                    "Alerta",
                    JOptionPane.WARNING_MESSAGE
                );
            }else{
                cliente = new ClsClientes(Id, CC, Nombres, Apellidos, Direccion, CorreoElectronico);
                setCliente(ctlCliente.Edit(cliente));
                JOptionPane.showMessageDialog(
                    this,
                    "Cliente editado!",
                    "Alerta",
                    JOptionPane.WARNING_MESSAGE
                );
            }
        }
        getClientes();
    }
    public void setCliente(ClsClientes cliente){
        clienteActual = cliente;
        btEliminarCliente.setEnabled(true);
        btEditarCliente.setEnabled(true);
        btVerNumeros.setEnabled(true);
        btCrearCliente.setEnabled(false);
        txtIdCliente.setText(""+cliente.getId());
        txtCcCliente.setText(""+cliente.getCC());
        txtNombresCliente.setText(""+cliente.getNombres());
        txtApellidosCliente.setText(""+cliente.getApellidos());
        txtDireccionCliente.setText(""+cliente.getDireccion());
        txtCorreoCliente.setText(""+cliente.getCorreoElectronico());
    }
    public void limpiarCliente(){
        clienteActual = null;
        btEliminarCliente.setEnabled(false);
        btEditarCliente.setEnabled(false);
        btVerNumeros.setEnabled(false);
        btCrearCliente.setEnabled(true);
        txtIdCliente.setText("");
        txtNombreCategoria.setText("");
        txtDescripcionCategoria.setText("");
        txtCcCliente.setText("");
        txtNombresCliente.setText("");
        txtApellidosCliente.setText("");
        txtDireccionCliente.setText("");
        txtCorreoCliente.setText("");
    }
    public void getClientes(){
        cliente_list = ctlCliente.All();
        DefaultTableModel model = (DefaultTableModel) tbClients.getModel();
        while(model.getRowCount()>=1) {
            model.removeRow(0);
        }
        String[] list_data = new String[6];
        cbClienteFactura.removeAllItems();
        for (ClsClientes cliente : cliente_list) {
            cbClienteFactura.addItem(cliente);
            list_data[0] = ""+cliente.getId();
            list_data[1] = ""+cliente.getCC();
            list_data[2] = ""+cliente.getNombres();
            list_data[3] = ""+cliente.getApellidos();
            list_data[4] = ""+cliente.getDireccion();
            list_data[5] = ""+cliente.getCorreoElectronico();
            model.addRow(list_data);
        }
        tbClients.setModel(model);
    }
    
    public void buscarCategoria(Integer id_categoria){
        ClsCategoria categoria = ctlCategoria.Search(id_categoria);
        setCategoria(categoria);
    }
    public void guardarCategoria(){
        ClsCategoria categoria;
        Integer Id = null;
        Integer productos = null;
        if(!txtIdCategoria.getText().equals("")){
            Id = Integer.parseInt(txtIdCategoria.getText());
            productos = Integer.parseInt(txtProductosCategoria.getText());
        }
        String Nombre = txtNombreCategoria.getText();
        String Descripcion = txtDescripcionCategoria.getText();
        if(Nombre.equals("")||Descripcion.equals("")){
            JOptionPane.showMessageDialog(
                this,
                "Completa los campos!",
                "Alerta",
                JOptionPane.WARNING_MESSAGE
            );
        }else{
            if(Id==null){
                categoria = new ClsCategoria(Nombre, Descripcion);
                setCategoria(ctlCategoria.Create(categoria));
                JOptionPane.showMessageDialog(
                    this,
                    "Categoria Creada!",
                    "Alerta",
                    JOptionPane.WARNING_MESSAGE
                );
            }else{
                categoria = new ClsCategoria(Id, Nombre, Descripcion, productos);
                setCategoria(ctlCategoria.Edit(categoria));
                JOptionPane.showMessageDialog(
                    this,
                    "Categoria Editada!",
                    "Alerta",
                    JOptionPane.WARNING_MESSAGE
                );
            }
        }
        getCategorias();
    }
    public void setCategoria(ClsCategoria categoria){
        btEliminarCategoria.setEnabled(true);
        btEditarCategoria.setEnabled(true);
        btCrearCategoria.setEnabled(false);
        txtIdCategoria.setText(""+categoria.getId());
        txtNombreCategoria.setText(""+categoria.getNombre());
        txtDescripcionCategoria.setText(""+categoria.getDescripcion());
        txtProductosCategoria.setText(""+categoria.getProductos());
    }
    public void limpiarCategoria(){
        btEliminarCategoria.setEnabled(false);
        btEditarCategoria.setEnabled(false);
        btCrearCategoria.setEnabled(true);
        txtIdCategoria.setText("");
        txtNombreCategoria.setText("");
        txtDescripcionCategoria.setText("");
        txtProductosCategoria.setText("");
    }
    public void getCategorias(){
        categoria_list = ctlCategoria.All();
        DefaultTableModel model = (DefaultTableModel) tbCategorias.getModel();
        while(model.getRowCount()>=1) {
            model.removeRow(0);
        }
        String[] list_data = new String[4];
        cbCategoriaProducto.removeAllItems();
        for (ClsCategoria categoria : categoria_list) {
            cbCategoriaProducto.addItem(categoria);
            list_data[0] = ""+categoria.getId();
            list_data[1] = ""+categoria.getNombre();
            list_data[2] = ""+categoria.getDescripcion();
            list_data[3] = ""+categoria.getProductos();
            model.addRow(list_data);
        }
        tbCategorias.setModel(model);
    }
    
    public void buscarProveedor(Integer id_proveedor){
        ClsProveedor proveedor = ctlProveedor.Search(id_proveedor);
        setProveedor(proveedor);
    }
    public void guardarProveedor(){
        ClsProveedor proveedor;
        Integer Id = null;
        Integer idDireccion = null;
        if(!txtIdProveedor.getText().equals("")){
            Id = Integer.parseInt(txtIdProveedor.getText());
            idDireccion = ctlProveedor.Search(Id).getIdDireccion();
        }
        String Nit = txtNitProveedor.getText();
        String RazonSocial = txtRazonSocialProveedor.getText();
        String Telefono = txtTelefonoProveedor.getText();
        String CorreoElectronico = txtCorreoElectronicoProveedor.getText();
        String RepresentanteLegal = txtRepresentanteLegarProveedor.getText();
        String SitioWeb = txtSitioWebProveedor.getText();
        String CalleCarrera = txtCalleCarreraProveedor.getText();
        String Numero = txtNumeroProveedor.getText();
        String Barrio = txtBarrioProveedor.getText();
        String Ciudad = txtCiudadProveedor.getText();
        if(Nit.equals("")||RazonSocial.equals("")||Telefono.equals("")
        ||CorreoElectronico.equals("")||RepresentanteLegal.equals("")
        ||SitioWeb.equals("")||CalleCarrera.equals("")||Numero.equals("")
        ||Barrio.equals("")||Ciudad.equals("")){
            JOptionPane.showMessageDialog(
                this,
                "Completa los campos!",
                "Alerta",
                JOptionPane.WARNING_MESSAGE
            );
        }else{
            if(Id==null){
                proveedor = new ClsProveedor(
                        Nit, RazonSocial, Telefono, CorreoElectronico,
                        RepresentanteLegal, SitioWeb, CalleCarrera, Numero, Barrio, Ciudad
                );
                setProveedor(ctlProveedor.Create(proveedor));
                JOptionPane.showMessageDialog(
                    this,
                    "Proveedor Creando!!!",
                    "Alerta",
                    JOptionPane.WARNING_MESSAGE
                );
            }else{
                proveedor = new ClsProveedor(
                        Id, Nit, RazonSocial, Telefono, CorreoElectronico,
                        RepresentanteLegal, SitioWeb, idDireccion,
                        CalleCarrera, Numero, Barrio, Ciudad
                );
                setProveedor(ctlProveedor.Edit(proveedor));
                JOptionPane.showMessageDialog(
                    this,
                    "Proveedor Editado!!!",
                    "Alerta",
                    JOptionPane.WARNING_MESSAGE
                );
            }
        }
        getProveedores();
    }
    public void setProveedor(ClsProveedor proveedor){
        btEliminarProveedor.setEnabled(true);
        btEditarProveedor.setEnabled(true);
        btCrearProveedor.setEnabled(false);
        txtIdProveedor.setText(""+proveedor.getId());
        txtNitProveedor.setText(""+proveedor.getNit());
        txtRazonSocialProveedor.setText(""+proveedor.getRazonSocial());
        txtTelefonoProveedor.setText(""+proveedor.getTelefono());
        txtCorreoElectronicoProveedor.setText(""+proveedor.getCorreoElectronico());
        txtRepresentanteLegarProveedor.setText(""+proveedor.getRepresentanteLegal());
        txtSitioWebProveedor.setText(""+proveedor.getSitioWeb());
        txtCalleCarreraProveedor.setText(""+proveedor.getCalleCarrera());
        txtNumeroProveedor.setText(""+proveedor.getNumero());
        txtBarrioProveedor.setText(""+proveedor.getBarrio());
        txtCiudadProveedor.setText(""+proveedor.getCiudad());
    }
    public void limpiarProveedor(){
        btEliminarProveedor.setEnabled(false);
        btEditarProveedor.setEnabled(false);
        btCrearProveedor.setEnabled(true);
        txtIdProveedor.setText("");
        txtNitProveedor.setText("");
        txtRazonSocialProveedor.setText("");
        txtTelefonoProveedor.setText("");
        txtCorreoElectronicoProveedor.setText("");
        txtRepresentanteLegarProveedor.setText("");
        txtSitioWebProveedor.setText("");
        txtCalleCarreraProveedor.setText("");
        txtNumeroProveedor.setText("");
        txtBarrioProveedor.setText("");
        txtCiudadProveedor.setText("");
    }
    public void getProveedores(){
        proveedor_list = ctlProveedor.All();
        DefaultTableModel model = (DefaultTableModel) tbProovedor.getModel();
        while(model.getRowCount()>=1) {
            model.removeRow(0);
        }
        
        String[] list_data = new String[11];
        cbProveedorProducto.removeAllItems();
        for (ClsProveedor proveedor : proveedor_list) {
            cbProveedorProducto.addItem(proveedor);
            list_data[0] = ""+proveedor.getId();
            list_data[1] = ""+proveedor.getNit();
            list_data[2] = ""+proveedor.getRazonSocial();
            list_data[3] = ""+proveedor.getTelefono();
            list_data[4] = ""+proveedor.getCorreoElectronico();
            list_data[5] = ""+proveedor.getRepresentanteLegal();
            list_data[6] = ""+proveedor.getSitioWeb();
            list_data[7] = ""+proveedor.getCalleCarrera();
            list_data[8] = ""+proveedor.getNumero();
            list_data[9] = ""+proveedor.getBarrio();
            list_data[10] = ""+proveedor.getCiudad();
            model.addRow(list_data);
        }
        tbProovedor.setModel(model);
    }
    
    public void pruebas(){
        btEliminarProveedor.setEnabled(false);
        btEditarProveedor.setEnabled(false);
        btCrearProveedor.setEnabled(true);
        txtIdProveedor.setText("");
        txtNitProveedor.setText("2342");
        txtRazonSocialProveedor.setText("sdvsd");
        txtTelefonoProveedor.setText("sdvsd");
        txtCorreoElectronicoProveedor.setText("sdvsd");
        txtRepresentanteLegarProveedor.setText("dsvsdv");
        txtSitioWebProveedor.setText("sdvs");
        txtCalleCarreraProveedor.setText("sdvs");
        txtNumeroProveedor.setText("sdvs");
        txtBarrioProveedor.setText("dvsdv");
        txtCiudadProveedor.setText("sdvsdv");
        
        txtNombreCategoria.setText("Categoria de prueba");
        txtDescripcionCategoria.setText("Descripcion de prueba");
        txtCcCliente.setText("1079932567");
        txtNombresCliente.setText("Juan Carlos");
        txtApellidosCliente.setText("Centeno");
        txtDireccionCliente.setText("Avenida Siempre Viva 5-34 2do Piso");
        txtCorreoCliente.setText("janca@centeno.com");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtIdProveedor = new javax.swing.JTextField();
        txtNitProveedor = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtRazonSocialProveedor = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtTelefonoProveedor = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtCorreoElectronicoProveedor = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtRepresentanteLegarProveedor = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtSitioWebProveedor = new javax.swing.JTextField();
        txtCalleCarreraProveedor = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtNumeroProveedor = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtBarrioProveedor = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtCiudadProveedor = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        btCrearProveedor = new javax.swing.JButton();
        btLimpiarProveedor = new javax.swing.JButton();
        btBuscarProveedor = new javax.swing.JButton();
        btEditarProveedor = new javax.swing.JButton();
        btEliminarProveedor = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbProovedor = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtIdCategoria = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtNombreCategoria = new javax.swing.JTextField();
        txtDescripcionCategoria = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        btCrearCategoria = new javax.swing.JButton();
        btLimpiarCategoria = new javax.swing.JButton();
        btBuscarCategoria = new javax.swing.JButton();
        btEditarCategoria = new javax.swing.JButton();
        btEliminarCategoria = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbCategorias = new javax.swing.JTable();
        txtProductosCategoria = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        txtIdProducto = new javax.swing.JTextField();
        txtNombreProducto = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        txtPrecioProducto = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txtExistenciaProducto = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        cbCategoriaProducto = new javax.swing.JComboBox<>();
        jLabel26 = new javax.swing.JLabel();
        cbProveedorProducto = new javax.swing.JComboBox<>();
        jLabel27 = new javax.swing.JLabel();
        btCrearProducto = new javax.swing.JButton();
        btLimpiarProducto = new javax.swing.JButton();
        btBuscarProducto = new javax.swing.JButton();
        btEditarProducto = new javax.swing.JButton();
        btEliminarProducto = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbProductos = new javax.swing.JTable();
        btAgregarExistencias = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        txtIdCliente = new javax.swing.JTextField();
        txtCcCliente = new javax.swing.JTextField();
        txtNombresCliente = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        txtApellidosCliente = new javax.swing.JTextField();
        txtDireccionCliente = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtCorreoCliente = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        btCrearCliente = new javax.swing.JButton();
        btLimpiarCliente = new javax.swing.JButton();
        btBuscarCliente = new javax.swing.JButton();
        btEditarCliente = new javax.swing.JButton();
        btEliminarCliente = new javax.swing.JButton();
        btVerNumeros = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbClients = new javax.swing.JTable();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbFacturas = new javax.swing.JTable();
        jLabel28 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        btNuevaFactura = new javax.swing.JButton();
        btBuscarFactura = new javax.swing.JButton();
        jLabel29 = new javax.swing.JLabel();
        cbClienteFactura = new javax.swing.JComboBox<>();
        jPanel7 = new javax.swing.JPanel();
        jlImagen = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        lbFactura = new javax.swing.JLabel();
        lbFecha = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        lbCC = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        lbCliente = new javax.swing.JLabel();
        rbNo = new javax.swing.JRadioButton();
        rbSi = new javax.swing.JRadioButton();
        jLabel36 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tbFactura = new javax.swing.JTable();
        lbIva = new javax.swing.JLabel();
        lbDescuento = new javax.swing.JLabel();
        lbTotal = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        btAgregarProducto = new javax.swing.JButton();
        btGuardarFactura = new javax.swing.JButton();
        jLabel33 = new javax.swing.JLabel();
        txtDescuentoFactura = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1224, 700));
        setPreferredSize(new java.awt.Dimension(1024, 700));

        jTabbedPane1.setMaximumSize(new java.awt.Dimension(32767, 650));

        jLabel1.setText("Id");

        txtIdProveedor.setEditable(false);
        txtIdProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdProveedorActionPerformed(evt);
            }
        });

        jLabel2.setText("Nit");

        txtRazonSocialProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRazonSocialProveedorActionPerformed(evt);
            }
        });

        jLabel3.setText("Razon Social");

        jLabel4.setText("Telefono");

        jLabel5.setText("Correo Electronico");

        jLabel6.setText("Representante Legal");

        jLabel7.setText("Sitio Web");

        jLabel8.setText("Calle / Carrera");

        jLabel9.setText("Numero");

        txtBarrioProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBarrioProveedorActionPerformed(evt);
            }
        });

        jLabel10.setText("Barrio");

        jLabel11.setText("Ciudad");

        btCrearProveedor.setText("Crear");
        btCrearProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCrearProveedorActionPerformed(evt);
            }
        });

        btLimpiarProveedor.setText("Limpiar");
        btLimpiarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLimpiarProveedorActionPerformed(evt);
            }
        });

        btBuscarProveedor.setText("Buscar");
        btBuscarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarProveedorActionPerformed(evt);
            }
        });

        btEditarProveedor.setText("Editar");
        btEditarProveedor.setEnabled(false);
        btEditarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEditarProveedorActionPerformed(evt);
            }
        });

        btEliminarProveedor.setText("Eliminar");
        btEliminarProveedor.setEnabled(false);
        btEliminarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEliminarProveedorActionPerformed(evt);
            }
        });

        tbProovedor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Id", "Nit", "Razon Social", "Telefono", "Correo Electronico", "Representante", "Sitio Web", "Calle/Carrera", "Numero", "Barrio", "Ciudad"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbProovedor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbProovedorMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbProovedor);
        if (tbProovedor.getColumnModel().getColumnCount() > 0) {
            tbProovedor.getColumnModel().getColumn(0).setMaxWidth(50);
            tbProovedor.getColumnModel().getColumn(8).setMaxWidth(50);
        }

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtSitioWebProveedor)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                                    .addComponent(txtIdProveedor))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtNitProveedor, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txtCalleCarreraProveedor)
                                .addComponent(txtRazonSocialProveedor, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE))
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtTelefonoProveedor)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtCorreoElectronicoProveedor)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtRepresentanteLegarProveedor)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                                    .addComponent(txtNumeroProveedor, javax.swing.GroupLayout.Alignment.LEADING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtBarrioProveedor)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtCiudadProveedor)))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btCrearProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btLimpiarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(btBuscarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btEditarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btEliminarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(660, 660, 660)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtIdProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNitProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRazonSocialProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTelefonoProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCorreoElectronicoProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRepresentanteLegarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSitioWebProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCalleCarreraProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNumeroProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBarrioProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCiudadProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btCrearProveedor)
                    .addComponent(btLimpiarProveedor)
                    .addComponent(btBuscarProveedor)
                    .addComponent(btEditarProveedor)
                    .addComponent(btEliminarProveedor))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 514, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Proveedores", jPanel1);

        jLabel12.setText("Id");

        txtIdCategoria.setEditable(false);
        txtIdCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdCategoriaActionPerformed(evt);
            }
        });

        jLabel13.setText("Nombre");

        jLabel14.setText("Descripción");

        btCrearCategoria.setText("Crear");
        btCrearCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCrearCategoriaActionPerformed(evt);
            }
        });

        btLimpiarCategoria.setText("Limpiar");
        btLimpiarCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLimpiarCategoriaActionPerformed(evt);
            }
        });

        btBuscarCategoria.setText("Buscar");
        btBuscarCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarCategoriaActionPerformed(evt);
            }
        });

        btEditarCategoria.setText("Editar");
        btEditarCategoria.setEnabled(false);
        btEditarCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEditarCategoriaActionPerformed(evt);
            }
        });

        btEliminarCategoria.setText("Eliminar");
        btEliminarCategoria.setEnabled(false);
        btEliminarCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEliminarCategoriaActionPerformed(evt);
            }
        });

        tbCategorias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Id", "Nombre", "Descripcion", "Productos"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbCategorias.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbCategoriasMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbCategorias);
        if (tbCategorias.getColumnModel().getColumnCount() > 0) {
            tbCategorias.getColumnModel().getColumn(0).setMinWidth(50);
            tbCategorias.getColumnModel().getColumn(0).setMaxWidth(50);
            tbCategorias.getColumnModel().getColumn(1).setMinWidth(150);
            tbCategorias.getColumnModel().getColumn(1).setMaxWidth(150);
            tbCategorias.getColumnModel().getColumn(3).setMinWidth(100);
            tbCategorias.getColumnModel().getColumn(3).setMaxWidth(100);
        }

        txtProductosCategoria.setEditable(false);

        jLabel15.setText("Productos");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1028, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btCrearCategoria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                            .addComponent(txtIdCategoria, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                                .addComponent(txtNombreCategoria))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btLimpiarCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btBuscarCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtDescripcionCategoria)
                                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtProductosCategoria)
                                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btEditarCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btEliminarCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtIdCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombreCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDescripcionCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProductosCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btCrearCategoria)
                    .addComponent(btLimpiarCategoria)
                    .addComponent(btBuscarCategoria)
                    .addComponent(btEditarCategoria)
                    .addComponent(btEliminarCategoria))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(166, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Categorias de productos", jPanel2);

        jLabel22.setText("Id");

        txtIdProducto.setEditable(false);

        jLabel23.setText("Nombre");

        jLabel24.setText("Precio");

        txtExistenciaProducto.setEditable(false);

        jLabel25.setText("Existencias");

        jLabel26.setText("Categoria");

        jLabel27.setText("Proveedor");

        btCrearProducto.setText("Crear");
        btCrearProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCrearProductoActionPerformed(evt);
            }
        });

        btLimpiarProducto.setText("Limpiar");
        btLimpiarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLimpiarProductoActionPerformed(evt);
            }
        });

        btBuscarProducto.setText("Buscar");
        btBuscarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarProductoActionPerformed(evt);
            }
        });

        btEditarProducto.setText("Editar");
        btEditarProducto.setEnabled(false);
        btEditarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEditarProductoActionPerformed(evt);
            }
        });

        btEliminarProducto.setText("Eliminar");
        btEliminarProducto.setEnabled(false);
        btEliminarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEliminarProductoActionPerformed(evt);
            }
        });

        tbProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Id", "Nombre", "Precio", "Existencias", "Vendidos", "Categoria", "Proveedor"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbProductosMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tbProductos);
        if (tbProductos.getColumnModel().getColumnCount() > 0) {
            tbProductos.getColumnModel().getColumn(0).setMinWidth(55);
            tbProductos.getColumnModel().getColumn(0).setMaxWidth(55);
            tbProductos.getColumnModel().getColumn(1).setMinWidth(200);
            tbProductos.getColumnModel().getColumn(1).setMaxWidth(200);
            tbProductos.getColumnModel().getColumn(2).setMinWidth(77);
            tbProductos.getColumnModel().getColumn(2).setMaxWidth(77);
            tbProductos.getColumnModel().getColumn(3).setMinWidth(70);
            tbProductos.getColumnModel().getColumn(3).setMaxWidth(70);
            tbProductos.getColumnModel().getColumn(4).setMinWidth(70);
            tbProductos.getColumnModel().getColumn(4).setMaxWidth(70);
        }

        btAgregarExistencias.setText("Agregar Existencias");
        btAgregarExistencias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAgregarExistenciasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(btCrearProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btLimpiarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btBuscarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btEditarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btEliminarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btAgregarExistencias))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtIdProducto, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                                    .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtNombreProducto)
                                    .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtPrecioProducto)
                                    .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtExistenciaProducto)
                                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cbCategoriaProducto, 0, 281, Short.MAX_VALUE)
                                    .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cbProveedorProducto, 0, 317, Short.MAX_VALUE)
                                    .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(9, 9, 9))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel22)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel24)
                        .addComponent(jLabel25)
                        .addComponent(jLabel26)
                        .addComponent(jLabel27)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtIdProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombreProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPrecioProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtExistenciaProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbCategoriaProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbProveedorProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btCrearProducto)
                    .addComponent(btLimpiarProducto)
                    .addComponent(btBuscarProducto)
                    .addComponent(btEditarProducto)
                    .addComponent(btEliminarProducto)
                    .addComponent(btAgregarExistencias))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(166, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Productos", jPanel4);

        jLabel16.setText("Id");

        txtIdCliente.setEditable(false);
        txtIdCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdClienteActionPerformed(evt);
            }
        });

        jLabel17.setText("CC");

        jLabel18.setText("Nombres");

        jLabel19.setText("Apellidos");

        jLabel20.setText("Direccion de entrega");

        jLabel21.setText("Correo Electronico");

        btCrearCliente.setText("Crear");
        btCrearCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCrearClienteActionPerformed(evt);
            }
        });

        btLimpiarCliente.setText("Limpiar");
        btLimpiarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLimpiarClienteActionPerformed(evt);
            }
        });

        btBuscarCliente.setText("Buscar");
        btBuscarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarClienteActionPerformed(evt);
            }
        });

        btEditarCliente.setText("Editar");
        btEditarCliente.setEnabled(false);
        btEditarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEditarClienteActionPerformed(evt);
            }
        });

        btEliminarCliente.setText("Eliminar");
        btEliminarCliente.setEnabled(false);
        btEliminarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEliminarClienteActionPerformed(evt);
            }
        });

        btVerNumeros.setText("Ver numeros de telefono");
        btVerNumeros.setEnabled(false);
        btVerNumeros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVerNumerosActionPerformed(evt);
            }
        });

        tbClients.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Id", "CC", "Nombres", "Apellidos", "Direccion", "Correo Electronico"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbClients.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbClientsMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbClients);
        if (tbClients.getColumnModel().getColumnCount() > 0) {
            tbClients.getColumnModel().getColumn(0).setMinWidth(50);
            tbClients.getColumnModel().getColumn(0).setMaxWidth(50);
            tbClients.getColumnModel().getColumn(1).setMinWidth(150);
            tbClients.getColumnModel().getColumn(1).setMaxWidth(150);
            tbClients.getColumnModel().getColumn(2).setMinWidth(150);
            tbClients.getColumnModel().getColumn(2).setMaxWidth(150);
            tbClients.getColumnModel().getColumn(3).setMinWidth(150);
            tbClients.getColumnModel().getColumn(3).setMaxWidth(150);
        }

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btCrearCliente, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                            .addComponent(txtIdCliente)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(btLimpiarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btBuscarCliente, javax.swing.GroupLayout.DEFAULT_SIZE, 63, Short.MAX_VALUE))
                            .addComponent(txtCcCliente)
                            .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txtNombresCliente, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel18, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(btEditarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btEliminarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtApellidosCliente)
                                    .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtDireccionCliente)
                                    .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtCorreoCliente)
                                    .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)))
                            .addComponent(btVerNumeros, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17)
                    .addComponent(jLabel18)
                    .addComponent(jLabel19)
                    .addComponent(jLabel20)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtIdCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCcCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombresCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtApellidosCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDireccionCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCorreoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btCrearCliente)
                    .addComponent(btLimpiarCliente)
                    .addComponent(btBuscarCliente)
                    .addComponent(btEditarCliente)
                    .addComponent(btEliminarCliente)
                    .addComponent(btVerNumeros))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 579, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Clientes", jPanel3);

        jSplitPane2.setDividerLocation(400);

        tbFacturas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "id", "Cliente", "Nit o CC", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbFacturas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbFacturasMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tbFacturas);
        if (tbFacturas.getColumnModel().getColumnCount() > 0) {
            tbFacturas.getColumnModel().getColumn(0).setMaxWidth(50);
            tbFacturas.getColumnModel().getColumn(3).setMaxWidth(100);
        }

        jLabel28.setFont(new java.awt.Font("Cantarell", 1, 24)); // NOI18N
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel28.setText("Lista de facturas");
        jLabel28.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel28)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE)
                .addContainerGap())
        );

        jSplitPane2.setLeftComponent(jPanel5);

        btNuevaFactura.setText("Nueva factura");
        btNuevaFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNuevaFacturaActionPerformed(evt);
            }
        });

        btBuscarFactura.setText("Buscar Factura");
        btBuscarFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarFacturaActionPerformed(evt);
            }
        });

        jLabel29.setText("Cliente");

        cbClienteFactura.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbClienteFacturaItemStateChanged(evt);
            }
        });
        cbClienteFactura.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cbClienteFacturaMouseEntered(evt);
            }
        });

        jPanel7.setBackground(new java.awt.Color(254, 254, 254));

        jLabel30.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(0, 127, 255));
        jLabel30.setText("Factura No: ");

        jLabel31.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(0, 127, 255));
        jLabel31.setText("Facha: ");

        lbFactura.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        lbFactura.setForeground(new java.awt.Color(1, 1, 1));

        lbFecha.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        lbFecha.setForeground(new java.awt.Color(1, 1, 1));

        jLabel32.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(0, 127, 255));
        jLabel32.setText("Nit/C.C.:");

        lbCC.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        lbCC.setForeground(new java.awt.Color(1, 1, 1));

        jLabel34.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        jLabel34.setForeground(new java.awt.Color(0, 127, 255));
        jLabel34.setText("Cliente: ");

        lbCliente.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        lbCliente.setForeground(new java.awt.Color(1, 1, 1));

        buttonGroup1.add(rbNo);
        rbNo.setForeground(new java.awt.Color(0, 128, 255));
        rbNo.setText("N");

        buttonGroup1.add(rbSi);
        rbSi.setForeground(new java.awt.Color(0, 127, 255));
        rbSi.setSelected(true);
        rbSi.setText("S");

        jLabel36.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(0, 127, 255));
        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel36.setText("Iva:  ");
        jLabel36.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        tbFactura.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 127, 255)));
        tbFactura.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Codigo", "Producto", "Cant.", "V.Unit", "Subtotal"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbFactura.setShowGrid(true);
        jScrollPane6.setViewportView(tbFactura);
        if (tbFactura.getColumnModel().getColumnCount() > 0) {
            tbFactura.getColumnModel().getColumn(0).setMinWidth(50);
            tbFactura.getColumnModel().getColumn(0).setMaxWidth(50);
            tbFactura.getColumnModel().getColumn(1).setMinWidth(250);
            tbFactura.getColumnModel().getColumn(2).setMinWidth(50);
            tbFactura.getColumnModel().getColumn(2).setMaxWidth(50);
            tbFactura.getColumnModel().getColumn(3).setMinWidth(100);
            tbFactura.getColumnModel().getColumn(3).setMaxWidth(100);
            tbFactura.getColumnModel().getColumn(4).setMinWidth(200);
            tbFactura.getColumnModel().getColumn(4).setMaxWidth(200);
        }

        lbIva.setFont(new java.awt.Font("Cantarell", 1, 18)); // NOI18N
        lbIva.setForeground(new java.awt.Color(1, 1, 1));
        lbIva.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 127, 255)));

        lbDescuento.setFont(new java.awt.Font("Cantarell", 1, 18)); // NOI18N
        lbDescuento.setForeground(new java.awt.Color(1, 1, 1));
        lbDescuento.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 127, 255)));

        lbTotal.setFont(new java.awt.Font("Cantarell", 1, 18)); // NOI18N
        lbTotal.setForeground(new java.awt.Color(1, 1, 1));
        lbTotal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 127, 255)));

        jLabel39.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        jLabel39.setForeground(new java.awt.Color(0, 127, 255));
        jLabel39.setText("Iva");

        jLabel40.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        jLabel40.setForeground(new java.awt.Color(0, 127, 255));
        jLabel40.setText("Descto");

        jLabel41.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        jLabel41.setForeground(new java.awt.Color(0, 127, 255));
        jLabel41.setText("Neto");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel7Layout.createSequentialGroup()
                                    .addComponent(jLabel30)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(lbFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel7Layout.createSequentialGroup()
                                    .addComponent(jLabel31)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(lbFecha, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(jLabel32)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lbCC, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jlImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel34)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel36, javax.swing.GroupLayout.DEFAULT_SIZE, 72, Short.MAX_VALUE)
                        .addGap(2, 2, 2)
                        .addComponent(rbSi)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rbNo))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel39, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel40)
                            .addComponent(jLabel41, javax.swing.GroupLayout.DEFAULT_SIZE, 72, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lbIva, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbDescuento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbTotal, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel30)
                            .addComponent(lbFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel31))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel32)
                            .addComponent(lbCC, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jlImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rbNo)
                            .addComponent(rbSi)
                            .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel34)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(lbCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(1, 1, 1)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel39, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbIva, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel40)
                    .addComponent(lbDescuento, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel41)
                    .addComponent(lbTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(141, 141, 141))
        );

        btAgregarProducto.setText("Agregar Producto");
        btAgregarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAgregarProductoActionPerformed(evt);
            }
        });

        btGuardarFactura.setText("Guardar Factura");
        btGuardarFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btGuardarFacturaActionPerformed(evt);
            }
        });

        jLabel33.setText("Descuento");

        txtDescuentoFactura.setEditable(false);
        txtDescuentoFactura.setText("0");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel29)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(btNuevaFactura)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btBuscarFactura))
                            .addComponent(cbClienteFactura, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(btAgregarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btGuardarFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel33, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)
                                .addComponent(txtDescuentoFactura, javax.swing.GroupLayout.Alignment.LEADING)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(jLabel33))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbClienteFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDescuentoFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btNuevaFactura)
                    .addComponent(btBuscarFactura)
                    .addComponent(btAgregarProducto)
                    .addComponent(btGuardarFactura))
                .addGap(18, 18, 18)
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jSplitPane2.setRightComponent(jPanel6);

        jTabbedPane1.addTab("Facturas", jSplitPane2);

        jTabbedPane1.setSelectedIndex(4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1072, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tbClientsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbClientsMouseClicked
        DefaultTableModel model = (DefaultTableModel)tbClients.getModel();
        int id = Integer.parseInt(""+model.getValueAt(tbClients.getSelectedRow(), 0));
        buscarCliente(id);
    }//GEN-LAST:event_tbClientsMouseClicked

    private void btVerNumerosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVerNumerosActionPerformed
        String numeros = "";
        for (ClsTelefonosClientes telefonosCliente : clienteActual.getTelefonosClientes()) {
            numeros+=telefonosCliente+"\n";
        }
        JTextField txtNuevoNumero = new JTextField();
        JComboBox cbNumeros = new JComboBox();
        for (ClsTelefonosClientes telefono : clienteActual.getTelefonosClientes()) {
            cbNumeros.addItem(telefono);
        }
        Object[] message = {
            numeros,"\n\n\n",
            "Seleccion: ", cbNumeros, "\n",
            "Nuemero de telefono:", txtNuevoNumero
        };
        String options[] = {
            "Guardar nuevo numero", "Actualizar Seleccion",
            "Eliminar seleccion", "Cancelar",
        };
        int option = JOptionPane.showOptionDialog(
            this, message, "Numeros de telefono",
            JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE,
            null, options, "Actualizar Seleccion"
        );
        if(option == -1||options[option]=="Cancelar"){
        }else if (options[option]=="Guardar nuevo numero" && !txtNuevoNumero.getText().equals("")) {
            ctlTelefono.Create(
                new ClsTelefonosClientes(clienteActual.getId(), txtNuevoNumero.getText())
            );
        }else if (options[option]=="Actualizar Seleccion" && !txtNuevoNumero.getText().equals("") && cbNumeros.getSelectedItem()!=null) {
            ClsTelefonosClientes telefono = (ClsTelefonosClientes)cbNumeros.getSelectedItem();
            telefono.setTelefono(txtNuevoNumero.getText());
            ctlTelefono.Edit(telefono);
        }else if (options[option]=="Eliminar seleccion" && cbNumeros.getSelectedItem()!=null) {
            ctlTelefono.Delete((ClsTelefonosClientes)cbNumeros.getSelectedItem());
            System.out.println("Eliminando numero: "+options[option]);
        }else{
            JOptionPane.showMessageDialog(
                this, "La ultima accion no se pudo realizar, verifique los campos",
                "Error", JOptionPane.ERROR_MESSAGE
            );
        }
        buscarCliente(clienteActual.getId());
    }//GEN-LAST:event_btVerNumerosActionPerformed

    private void btEliminarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEliminarClienteActionPerformed
        boolean confirm = ctlCliente.Delete(
            ctlCliente.Search(Integer.parseInt(txtIdCliente.getText()))
        );
        if(confirm){
            JOptionPane.showMessageDialog(this, "Cliente Eliminado");
            getClientes();
            limpiarCliente();
        }else{
            JOptionPane.showMessageDialog(this, "El cliente no puede ser eliminado si tiene numeros de telefono");
        }
    }//GEN-LAST:event_btEliminarClienteActionPerformed

    private void btEditarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEditarClienteActionPerformed
        guardarCliente();
    }//GEN-LAST:event_btEditarClienteActionPerformed

    private void btBuscarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarClienteActionPerformed
        try {
            int id = Integer.parseInt(JOptionPane.showInputDialog(this, "Ingrese el id del cliente"));
            ClsClientes cliente = this.ctlCliente.Search(id);
            if(cliente!=null){
                setCliente(cliente);
            }else{
                JOptionPane.showMessageDialog(this, "El cliente no existe!", "WARNING!", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Id no valido!!", "Error!", JOptionPane.ERROR_MESSAGE);

        }
    }//GEN-LAST:event_btBuscarClienteActionPerformed

    private void btLimpiarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLimpiarClienteActionPerformed
        limpiarCliente();
    }//GEN-LAST:event_btLimpiarClienteActionPerformed

    private void btCrearClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCrearClienteActionPerformed
        guardarCliente();
    }//GEN-LAST:event_btCrearClienteActionPerformed

    private void txtIdClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdClienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdClienteActionPerformed

    private void btAgregarExistenciasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAgregarExistenciasActionPerformed
        Integer cant_actual = Integer.parseInt(txtExistenciaProducto.getText());
        try {
            int cant_adicional = Integer.parseInt(JOptionPane.showInputDialog(this, "Ingrese la cantidad a agregar"));
            txtExistenciaProducto.setText(""+(cant_actual+cant_adicional));
            guardarProducto();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Cantidad no valida!!", "Error!", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btAgregarExistenciasActionPerformed

    private void tbProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbProductosMouseClicked
        DefaultTableModel model = (DefaultTableModel)tbProductos.getModel();
        int id = Integer.parseInt(""+model.getValueAt(tbProductos.getSelectedRow(), 0));
        buscarProducto(id);
    }//GEN-LAST:event_tbProductosMouseClicked

    private void btEliminarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEliminarProductoActionPerformed
        boolean confirm = ctlProducto.Delete(
            ctlProducto.Search(Integer.parseInt(txtIdProducto.getText()))
        );
        if(confirm){
            JOptionPane.showMessageDialog(this, "Producto Eliminado");
            getProductos();
            limpiarProducto();
        }else{
            JOptionPane.showMessageDialog(this, "El producto no puede ser eliminado si tiene ventas asociadas");
        }
    }//GEN-LAST:event_btEliminarProductoActionPerformed

    private void btEditarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEditarProductoActionPerformed
        guardarProducto();
    }//GEN-LAST:event_btEditarProductoActionPerformed

    private void btBuscarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarProductoActionPerformed
        try {
            int id = Integer.parseInt(JOptionPane.showInputDialog(this, "Ingrese el id del Producto"));
            ClsProductos producto = this.ctlProducto.Search(id);
            if(producto!=null){
                setProducto(producto);
            }else{
                JOptionPane.showMessageDialog(this, "El producto no existe!", "WARNING!", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Id no valido!!", "Error!", JOptionPane.ERROR_MESSAGE);

        }
    }//GEN-LAST:event_btBuscarProductoActionPerformed

    private void btLimpiarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLimpiarProductoActionPerformed
        limpiarProducto();
    }//GEN-LAST:event_btLimpiarProductoActionPerformed

    private void btCrearProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCrearProductoActionPerformed
        guardarProducto();
    }//GEN-LAST:event_btCrearProductoActionPerformed

    private void tbCategoriasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbCategoriasMouseClicked
        DefaultTableModel model = (DefaultTableModel)tbCategorias.getModel();
        int id = Integer.parseInt(""+model.getValueAt(tbCategorias.getSelectedRow(), 0));
        buscarCategoria(id);
    }//GEN-LAST:event_tbCategoriasMouseClicked

    private void btEliminarCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEliminarCategoriaActionPerformed
        boolean confirm = ctlCategoria.Delete(
            ctlCategoria.Search(Integer.parseInt(txtIdCategoria.getText()))
        );
        if(confirm){
            JOptionPane.showMessageDialog(this, "Categoria Eliminada");
            getCategorias();
            limpiarCategoria();
        }else{
            JOptionPane.showMessageDialog(this, "La categoria no puede ser eliminada si tiene productos");
        }
    }//GEN-LAST:event_btEliminarCategoriaActionPerformed

    private void btEditarCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEditarCategoriaActionPerformed
        guardarCategoria();
    }//GEN-LAST:event_btEditarCategoriaActionPerformed

    private void btBuscarCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarCategoriaActionPerformed
        try {
            int id = Integer.parseInt(JOptionPane.showInputDialog(this, "Ingrese el id de la categoria"));
            ClsCategoria categoria = this.ctlCategoria.Search(id);
            if(categoria!=null){
                setCategoria(categoria);
            }else{
                JOptionPane.showMessageDialog(this, "La categoria no existe!", "WARNING!", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Id no valido!!", "Error!", JOptionPane.ERROR_MESSAGE);

        }
    }//GEN-LAST:event_btBuscarCategoriaActionPerformed

    private void btLimpiarCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLimpiarCategoriaActionPerformed
        limpiarCategoria();
    }//GEN-LAST:event_btLimpiarCategoriaActionPerformed

    private void btCrearCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCrearCategoriaActionPerformed
        guardarCategoria();
    }//GEN-LAST:event_btCrearCategoriaActionPerformed

    private void txtIdCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdCategoriaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdCategoriaActionPerformed

    private void tbProovedorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbProovedorMouseClicked
        DefaultTableModel model = (DefaultTableModel)tbProovedor.getModel();
        int id = Integer.parseInt(""+model.getValueAt(tbProovedor.getSelectedRow(), 0));
        buscarProveedor(id);
    }//GEN-LAST:event_tbProovedorMouseClicked

    private void btEliminarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEliminarProveedorActionPerformed
        boolean confirm = ctlProveedor.Delete(
            ctlProveedor.Search(Integer.parseInt(txtIdProveedor.getText()))
        );
        if(confirm){
            JOptionPane.showMessageDialog(this, "Proveedor eliminado");
            getProveedores();
            limpiarProveedor();
        }
    }//GEN-LAST:event_btEliminarProveedorActionPerformed

    private void btEditarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEditarProveedorActionPerformed
        guardarProveedor();
    }//GEN-LAST:event_btEditarProveedorActionPerformed

    private void btBuscarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarProveedorActionPerformed
        try {
            int id = Integer.parseInt(JOptionPane.showInputDialog(this, "Ingrese el id del proveedor"));
            ClsProveedor proveedor = this.ctlProveedor.Search(id);
            if(proveedor!=null){
                setProveedor(proveedor);
            }else{
                JOptionPane.showMessageDialog(this, "El proveedor no existe!", "WARNING!", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Id no valido!!", "Error!", JOptionPane.ERROR_MESSAGE);

        }
    }//GEN-LAST:event_btBuscarProveedorActionPerformed

    private void btLimpiarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLimpiarProveedorActionPerformed
        limpiarProveedor();
    }//GEN-LAST:event_btLimpiarProveedorActionPerformed

    private void btCrearProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCrearProveedorActionPerformed
        guardarProveedor();
    }//GEN-LAST:event_btCrearProveedorActionPerformed

    private void txtBarrioProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBarrioProveedorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBarrioProveedorActionPerformed

    private void txtRazonSocialProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRazonSocialProveedorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRazonSocialProveedorActionPerformed

    private void txtIdProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdProveedorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdProveedorActionPerformed

    private void btNuevaFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNuevaFacturaActionPerformed
        limpiarFactura();
    }//GEN-LAST:event_btNuevaFacturaActionPerformed

    private void tbFacturasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbFacturasMouseClicked
        DefaultTableModel model = (DefaultTableModel)tbFacturas.getModel();
        int id = Integer.parseInt(""+model.getValueAt(tbFacturas.getSelectedRow(), 0));
        buscarFactura(id);
    }//GEN-LAST:event_tbFacturasMouseClicked

    private void btBuscarFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarFacturaActionPerformed
        try {
            int id = Integer.parseInt(JOptionPane.showInputDialog(this, "Ingrese el Numero de la factura"));
            ClsFactura factura = this.ctlFactura.Search(id);
            if(factura!=null){
                setFactura(factura);
            }else{
                JOptionPane.showMessageDialog(this, "La factura no existe!", "WARNING!", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Numero no valido!!", "Error!", JOptionPane.ERROR_MESSAGE);

        }
    }//GEN-LAST:event_btBuscarFacturaActionPerformed

    private void btAgregarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAgregarProductoActionPerformed
        try {
            Object[] message = {
                "Producto: ", cbSelectProducto, "\n",
                "Cantidad:"
            };
            ///TODO: Posible excepcion por conversion a entero
            Integer option = Integer.parseInt(
                JOptionPane.showInputDialog(
                    this, 
                    message, 
                    "Seleccione un producto", JOptionPane.INFORMATION_MESSAGE
                )
            );
            ClsProductos productoSelect = (ClsProductos) cbSelectProducto.getSelectedItem();
            ClsProductoFactura agregarProducto = new ClsProductoFactura(
                productoSelect.getPrecioActual(),
                option,
                null,
                productoSelect.getId(),
                productoSelect.getNombre(),
                productoSelect.getExistencia()
            );
            facturaActual.setProducto(agregarProducto);
            setFactura(facturaActual);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Ingrese una cantidad valida!", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btAgregarProductoActionPerformed

    private void btGuardarFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btGuardarFacturaActionPerformed
        guardarFactura();
    }//GEN-LAST:event_btGuardarFacturaActionPerformed

    private void cbClienteFacturaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbClienteFacturaMouseEntered
        
    }//GEN-LAST:event_cbClienteFacturaMouseEntered

    private void cbClienteFacturaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbClienteFacturaItemStateChanged
       
    }//GEN-LAST:event_cbClienteFacturaItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmMain().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAgregarExistencias;
    private javax.swing.JButton btAgregarProducto;
    private javax.swing.JButton btBuscarCategoria;
    private javax.swing.JButton btBuscarCliente;
    private javax.swing.JButton btBuscarFactura;
    private javax.swing.JButton btBuscarProducto;
    private javax.swing.JButton btBuscarProveedor;
    private javax.swing.JButton btCrearCategoria;
    private javax.swing.JButton btCrearCliente;
    private javax.swing.JButton btCrearProducto;
    private javax.swing.JButton btCrearProveedor;
    private javax.swing.JButton btEditarCategoria;
    private javax.swing.JButton btEditarCliente;
    private javax.swing.JButton btEditarProducto;
    private javax.swing.JButton btEditarProveedor;
    private javax.swing.JButton btEliminarCategoria;
    private javax.swing.JButton btEliminarCliente;
    private javax.swing.JButton btEliminarProducto;
    private javax.swing.JButton btEliminarProveedor;
    private javax.swing.JButton btGuardarFactura;
    private javax.swing.JButton btLimpiarCategoria;
    private javax.swing.JButton btLimpiarCliente;
    private javax.swing.JButton btLimpiarProducto;
    private javax.swing.JButton btLimpiarProveedor;
    private javax.swing.JButton btNuevaFactura;
    private javax.swing.JButton btVerNumeros;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<ClsCategoria> cbCategoriaProducto;
    private javax.swing.JComboBox<ClsClientes> cbClienteFactura;
    private javax.swing.JComboBox<ClsProveedor> cbProveedorProducto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel jlImagen;
    private javax.swing.JLabel lbCC;
    private javax.swing.JLabel lbCliente;
    private javax.swing.JLabel lbDescuento;
    private javax.swing.JLabel lbFactura;
    private javax.swing.JLabel lbFecha;
    private javax.swing.JLabel lbIva;
    private javax.swing.JLabel lbTotal;
    private javax.swing.JRadioButton rbNo;
    private javax.swing.JRadioButton rbSi;
    private javax.swing.JTable tbCategorias;
    private javax.swing.JTable tbClients;
    private javax.swing.JTable tbFactura;
    private javax.swing.JTable tbFacturas;
    private javax.swing.JTable tbProductos;
    private javax.swing.JTable tbProovedor;
    private javax.swing.JTextField txtApellidosCliente;
    private javax.swing.JTextField txtBarrioProveedor;
    private javax.swing.JTextField txtCalleCarreraProveedor;
    private javax.swing.JTextField txtCcCliente;
    private javax.swing.JTextField txtCiudadProveedor;
    private javax.swing.JTextField txtCorreoCliente;
    private javax.swing.JTextField txtCorreoElectronicoProveedor;
    private javax.swing.JTextField txtDescripcionCategoria;
    private javax.swing.JTextField txtDescuentoFactura;
    private javax.swing.JTextField txtDireccionCliente;
    private javax.swing.JTextField txtExistenciaProducto;
    private javax.swing.JTextField txtIdCategoria;
    private javax.swing.JTextField txtIdCliente;
    private javax.swing.JTextField txtIdProducto;
    private javax.swing.JTextField txtIdProveedor;
    private javax.swing.JTextField txtNitProveedor;
    private javax.swing.JTextField txtNombreCategoria;
    private javax.swing.JTextField txtNombreProducto;
    private javax.swing.JTextField txtNombresCliente;
    private javax.swing.JTextField txtNumeroProveedor;
    private javax.swing.JTextField txtPrecioProducto;
    private javax.swing.JTextField txtProductosCategoria;
    private javax.swing.JTextField txtRazonSocialProveedor;
    private javax.swing.JTextField txtRepresentanteLegarProveedor;
    private javax.swing.JTextField txtSitioWebProveedor;
    private javax.swing.JTextField txtTelefonoProveedor;
    // End of variables declaration//GEN-END:variables
}
