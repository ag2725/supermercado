/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import classes.ClsProveedor;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import static java.sql.Statement.RETURN_GENERATED_KEYS;
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author ever
 */
public class ModelProveedor {
    DbData dbData;
    ClsProveedor proveedor;
    public ModelProveedor() {
        this.dbData = new DbData();
        try {
            Class.forName(dbData.getDriver());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    public ClsProveedor Create(ClsProveedor proveedor){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "INSERT INTO tb_direcciones(CalleCarrera, Numero, Barrio, Ciudad)\n" +
                            "VALUES(?, ?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(query, RETURN_GENERATED_KEYS);
            statement.setString(1, proveedor.getCalleCarrera());
            statement.setString(2, proveedor.getNumero());
            statement.setString(3, proveedor.getBarrio());
            statement.setString(4, proveedor.getCiudad());
            int rowInsert = statement.executeUpdate();
            int idDireccion=6;
            if(rowInsert>0){
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if(generatedKeys.next()){
                    idDireccion = generatedKeys.getInt(1);
                }
            }
            query = "INSERT INTO tb_proveedores(Nit, RazonSocial, Telefono, CorreoElectronico, RepresentanteLegal, SitioWeb, idDireccion)\n" +
                    "VALUES(?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement statementPro = conn.prepareStatement(query, RETURN_GENERATED_KEYS);
            statementPro.setString(1, proveedor.getNit());
            statementPro.setString(2, proveedor.getRazonSocial());
            statementPro.setString(3, proveedor.getTelefono());
            statementPro.setString(4, proveedor.getCorreoElectronico());
            statementPro.setString(5, proveedor.getRepresentanteLegal());
            statementPro.setString(6, proveedor.getSitioWeb());
            statementPro.setInt(7, idDireccion);
            rowInsert = statementPro.executeUpdate();
            int idProveedor=0;
            if(rowInsert>0){
                ResultSet generatedKeys = statementPro.getGeneratedKeys();
                if(generatedKeys.next()){
                    idProveedor = generatedKeys.getInt(1);
                }
            }
            return Search(idProveedor);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
    public ClsProveedor Edit(ClsProveedor proveedor){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "UPDATE tb_proveedores AS pro INNER JOIN tb_direcciones AS dir\n" +
                            "ON pro.idDireccion=dir.Id\n" +
                            "SET pro.Nit=?, pro.RazonSocial=?, pro.Telefono=?, pro.CorreoElectronico=?,\n" +
                            "pro.RepresentanteLegal=?, pro.SitioWeb=?, pro.idDireccion=?,\n" +
                            "dir.CalleCarrera=?, dir.Numero=?, dir.Barrio=?, dir.Ciudad=?\n" +
                            "WHERE pro.Id=?;";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, proveedor.getNit());
            statement.setString(2, proveedor.getRazonSocial());
            statement.setString(3, proveedor.getTelefono());
            statement.setString(4, proveedor.getCorreoElectronico());
            statement.setString(5, proveedor.getRepresentanteLegal());
            statement.setString(6, proveedor.getSitioWeb());
            statement.setInt(7, proveedor.getIdDireccion());
            statement.setString(8, proveedor.getCalleCarrera());
            statement.setString(9, proveedor.getNumero());
            statement.setString(10, proveedor.getBarrio());
            statement.setString(11, proveedor.getCiudad());
            statement.setInt(12, proveedor.getId());
            statement.executeUpdate();
            return  Search(proveedor.getId());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
    public boolean Delete(ClsProveedor proveedor){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "DELETE FROM tb_proveedores WHERE id=?";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(1, proveedor.getId());
            statement.executeUpdate();
            query = "DELETE FROM tb_direcciones WHERE id=?";
            statement = conn.prepareStatement(query);
            statement.setInt(1, proveedor.getIdDireccion());
            int rowDelete = statement.executeUpdate();
            if(rowDelete>0){
               return true; 
            }else{
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
    public ClsProveedor Search(Integer idProveedor){
        proveedor = null;
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "SELECT pro.Id, pro.Nit, pro.RazonSocial, pro.Telefono, pro.CorreoElectronico,\n" +
                            "pro.RepresentanteLegal, pro.SitioWeb, pro.idDireccion,\n" +
                            "dir.CalleCarrera, dir.Numero, dir.Barrio, dir.Ciudad FROM tb_proveedores AS pro\n" +
                            "INNER JOIN tb_direcciones AS dir ON pro.idDireccion=dir.Id WHERE pro.Id=?;";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(1, idProveedor);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                Integer Id = result.getInt("Id");
                String Nit = result.getString("Nit");
                String RazonSocial = result.getString("RazonSocial");
                String Telefono = result.getString("Telefono");
                String CorreoElectronico = result.getString("CorreoElectronico");
                String RepresentanteLegal = result.getString("RepresentanteLegal");
                String SitioWeb = result.getString("SitioWeb");
                Integer idDireccion = result.getInt("idDireccion");
                String CalleCarrera = result.getString("CalleCarrera");
                String Numero = result.getString("Numero");
                String Barrio = result.getString("Barrio");
                String Ciudad = result.getString("Ciudad");
                proveedor = new ClsProveedor(
                    Id, Nit, RazonSocial, Telefono, CorreoElectronico,
                    RepresentanteLegal, SitioWeb, idDireccion, CalleCarrera,
                    Numero, Barrio, Ciudad
                );
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
        return proveedor;
    }
     
    public LinkedList<ClsProveedor> All(){
        LinkedList<ClsProveedor> proveedor_list = new LinkedList<>();
        proveedor = null;
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "SELECT pro.Id, pro.Nit, pro.RazonSocial, pro.Telefono, pro.CorreoElectronico,\n" +
                            "pro.RepresentanteLegal, pro.SitioWeb, pro.idDireccion,\n" +
                            "dir.CalleCarrera, dir.Numero, dir.Barrio, dir.Ciudad FROM tb_proveedores AS pro\n" +
                            "INNER JOIN tb_direcciones AS dir ON pro.idDireccion=dir.Id;";
            PreparedStatement statement = conn.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                Integer Id = result.getInt("Id");
                String Nit = result.getString("Nit");
                String RazonSocial = result.getString("RazonSocial");
                String Telefono = result.getString("Telefono");
                String CorreoElectronico = result.getString("CorreoElectronico");
                String RepresentanteLegal = result.getString("RepresentanteLegal");
                String SitioWeb = result.getString("SitioWeb");
                Integer idDireccion = result.getInt("idDireccion");
                String CalleCarrera = result.getString("CalleCarrera");
                String Numero = result.getString("Numero");
                String Barrio = result.getString("Barrio");
                String Ciudad = result.getString("Ciudad");
                proveedor = new ClsProveedor(
                    Id, Nit, RazonSocial, Telefono, CorreoElectronico,
                    RepresentanteLegal, SitioWeb, idDireccion, CalleCarrera,
                    Numero, Barrio, Ciudad
                );
                proveedor_list.add(proveedor);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
        return proveedor_list;
    }
}
