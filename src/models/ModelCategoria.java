/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import classes.ClsCategoria;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import static java.sql.Statement.RETURN_GENERATED_KEYS;
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author ever
 */
public class ModelCategoria {
    DbData dbData;
    ClsCategoria categoria;
    public ModelCategoria() {
        this.dbData = new DbData();
        try {
            Class.forName(dbData.getDriver());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    public ClsCategoria Create(ClsCategoria categoria){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "INSERT INTO tb_categoria(Nombre, Descripcion)\n" +
                            "VALUES(?, ?)";
            PreparedStatement statement = conn.prepareStatement(query, RETURN_GENERATED_KEYS);
            statement.setString(1, categoria.getNombre());
            statement.setString(2, categoria.getDescripcion());
            int rowInsert = statement.executeUpdate();
            int idDireccion=0;
            if(rowInsert>0){
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if(generatedKeys.next()){
                    idDireccion = generatedKeys.getInt(1);
                }
            }
            return Search(idDireccion);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
    public ClsCategoria Edit(ClsCategoria categoria){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "UPDATE tb_categoria\n" +
                            "SET Nombre=?, Descripcion=? WHERE Id=?;";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, categoria.getNombre());
            statement.setString(2, categoria.getDescripcion());
            statement.setInt(3, categoria.getId());
            statement.executeUpdate();
            return Search(categoria.getId());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
    public boolean Delete(ClsCategoria categoria){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "DELETE FROM tb_categoria WHERE Id=?";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(1, categoria.getId());
            int rowDelete = statement.executeUpdate();
            if(rowDelete>0){
               return true; 
            }else{
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
    public ClsCategoria Search(Integer idCategoria){
        categoria = null;
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "SELECT cat.Id, cat.Nombre, cat.Descripcion,\n" +
                            "(SELECT COUNT(*) FROM tb_productos AS pro WHERE pro.idCategoria=cat.Id) AS productos\n" +
                            "FROM tb_categoria AS cat WHERE Id=?;";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(1, idCategoria);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                Integer Id = result.getInt("Id");
                String Nombre = result.getString("Nombre");
                String Descripcion = result.getString("Descripcion");
                Integer productos = result.getInt("productos");
                categoria = new ClsCategoria(Id, Nombre, Descripcion, productos);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
        return categoria;
    }
     
    public LinkedList<ClsCategoria> All(){
        LinkedList<ClsCategoria> categoria_list = new LinkedList<>();
        categoria = null;
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "SELECT cat.Id, cat.Nombre, cat.Descripcion,\n" +
                            "(SELECT COUNT(*) FROM tb_productos AS pro WHERE pro.idCategoria=cat.Id) AS productos\n" +
                            "FROM tb_categoria AS cat;";
            PreparedStatement statement = conn.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                Integer Id = result.getInt("Id");
                String Nombre = result.getString("Nombre");
                String Descripcion = result.getString("Descripcion");
                Integer productos = result.getInt("productos");
                categoria = new ClsCategoria(Id, Nombre, Descripcion, productos);
                categoria_list.add(categoria);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
        return categoria_list;
    }
}
