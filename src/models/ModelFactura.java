package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import static java.sql.Statement.RETURN_GENERATED_KEYS;
import java.util.Date;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import classes.ClsFactura;
import classes.ClsProductoFactura;

public class ModelFactura {
    DbData dbData;
    ClsFactura factura;
    public ModelFactura() {
        this.dbData = new DbData();
        try {
            Class.forName(dbData.getDriver());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    public ClsFactura Create(ClsFactura factura){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "INSERT INTO tb_facturas(idClient, Descuento, Total)\n" +
                            "VALUES(?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(query, RETURN_GENERATED_KEYS);
            statement.setInt(1, factura.getIdClient());
            statement.setInt(2, factura.getDescuento());
            statement.setFloat(3, factura.getTotal());
            int rowInsert = statement.executeUpdate();
            int idFactura=0;
            if(rowInsert>0){
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if(generatedKeys.next()){
                    idFactura = generatedKeys.getInt(1);
                }
            }
            //Creando Productos
            for (ClsProductoFactura producto : factura.getProductos()) {
                query = "INSERT INTO tb_productos_to_facturas(PrecioUnitario, Cantidad, idFactura, idProducto)\n" +
                        "VALUES(?, ?, ?, ?)";
                statement = conn.prepareStatement(query);
                statement.setFloat(1, producto.getPrecioUnitario());
                statement.setInt(2, producto.getCantidad());
                statement.setInt(3, idFactura);
                statement.setInt(4, producto.getIdProducto());
                statement.executeUpdate();
                //Descontando de inventario
                query = "UPDATE tb_productos SET Existencia=(tb_productos.Existencia-?) WHERE Id=?;";
                statement = conn.prepareStatement(query);
                statement.setFloat(1, producto.getCantidad());
                statement.setInt(2, producto.getIdProducto());
                statement.executeUpdate();
                //Fin Descontando de inventario
            }
            //Creando fin productos
            //JOptionPane.showMessageDialog(null, "Crear productos y descontar inventario", "title", JOptionPane.INFORMATION_MESSAGE);
            return Search(idFactura);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
    public ClsFactura Edit(ClsFactura factura){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "UPDATE tb_facturas SET idClient=?, Descuento=?, Total=? WHERE Id=?;";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(1, factura.getIdClient());
            statement.setInt(2, factura.getDescuento());
            statement.setFloat(3, factura.getTotal());
            statement.setInt(4, factura.getId());
            statement.executeUpdate();
            //Productos
            for (ClsProductoFactura producto : factura.getProductos()) {
                if(producto.getId()!=null){
                    query = "UPDATE tb_productos_to_facturas SET PrecioUnitario=?, Cantidad=?, idFactura=?, idProducto=? WHERE Id=?;";
                    statement = conn.prepareStatement(query);
                    statement.setFloat(1, producto.getPrecioUnitario());
                    statement.setInt(2, producto.getCantidad());
                    statement.setInt(3, factura.getId());
                    statement.setInt(4, producto.getIdProducto());
                    statement.setInt(5, producto.getId());
                    statement.executeUpdate();
                }else{
                    query = "INSERT INTO tb_productos_to_facturas(PrecioUnitario, Cantidad, idFactura, idProducto)\n" +
                            "VALUES(?, ?, ?, ?)";
                    statement = conn.prepareStatement(query);
                    statement.setFloat(1, producto.getPrecioUnitario());
                    statement.setInt(2, producto.getCantidad());
                    statement.setInt(3, factura.getId());
                    statement.setInt(4, producto.getIdProducto());
                    statement.executeUpdate();
                    //Descontando de inventario
                    query = "UPDATE tb_productos SET Existencia=(tb_productos.Existencia-?) WHERE Id=?;";
                    statement = conn.prepareStatement(query);
                    statement.setFloat(1, producto.getCantidad());
                    statement.setInt(2, producto.getIdProducto());
                    statement.executeUpdate();
                    //Fin Descontando de inventario
                }
            }
            //fin productos
            return Search(factura.getId());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
    public boolean Delete(ClsFactura factura){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "DELETE FROM tb_facturas WHERE Id=?";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(1, factura.getId());
            int rowDelete = statement.executeUpdate();
            if(rowDelete>0){
               return true; 
            }else{
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
    public ClsFactura Search(Integer idFactura){
        factura = null;
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "SELECT fac.Id, fac.Fecha, fac.idClient, fac.Descuento, fac.Total, cli.Nombres, cli.Apellidos, cli.CC\n" +
                            "FROM tb_facturas AS fac\n" +
                            "INNER JOIN tb_clientes AS cli\n" +
                            "ON fac.idClient=cli.Id\n" +
                            "WHERE fac.Id=?\n" +
                            "GROUP BY fac.Id;";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(1, idFactura);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                Integer Id = result.getInt("Id");
                Date Fecha = result.getDate("Fecha");
                Integer idClient = result.getInt("idClient");
                Integer Descuento = result.getInt("Descuento");
                Float Total = result.getFloat("Total");
                String Cliente = result.getString("Apellidos")+" "+result.getString("Nombres");
                String CC = result.getString("CC");
                factura = new ClsFactura(Id, Fecha, idClient, Descuento, Total, Cliente, CC);
            }
            query = "SELECT prof.Id, prof.PrecioUnitario, prof.Cantidad, prof.idProducto, pro.Nombre, pro.Existencia, pro.PrecioActual\n" +
                    "FROM tb_productos_to_facturas AS prof\n" +
                    "INNER JOIN tb_productos AS pro\n" +
                    "ON prof.idProducto=pro.Id\n" +
                    "WHERE prof.idFactura=?\n" +
                    "GROUP BY prof.Id;";
            statement = conn.prepareStatement(query);
            statement.setInt(1, idFactura);
            result = statement.executeQuery();
            while (result.next()){
                Integer Id = result.getInt("Id");
                Float PrecioUnitario = result.getFloat("PrecioUnitario")>0?result.getFloat("PrecioUnitario"):result.getFloat("PrecioActual");
                Integer Cantidad = result.getInt("Cantidad");
                Integer idProducto = result.getInt("idProducto");
                String Nombre = result.getString("Nombre");
                Integer Existencia = result.getInt("Existencia");
                ClsProductoFactura  ProductoFactura = new ClsProductoFactura(
                    Id, PrecioUnitario, Cantidad, idFactura, idProducto, Nombre, Existencia
                );
                factura.setProducto(ProductoFactura);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
        return factura;
    }
     
    public LinkedList<ClsFactura> All(){
        LinkedList<ClsFactura> factura_list = new LinkedList<>();
        factura = null;
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "SELECT fac.Id, fac.Fecha, fac.idClient, fac.Descuento, fac.Total, cli.Nombres, cli.Apellidos, cli.CC\n" +
                            "FROM tb_facturas AS fac\n" +
                            "INNER JOIN tb_clientes AS cli\n" +
                            "ON fac.idClient=cli.Id\n" +
                            "GROUP BY fac.Id;";
            PreparedStatement statement = conn.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                Integer Id = result.getInt("Id");
                Date Fecha = result.getDate("Fecha");
                Integer idClient = result.getInt("idClient");
                Integer Descuento = result.getInt("Descuento");
                Float Total = result.getFloat("Total");
                String Cliente = result.getString("Apellidos")+""+result.getString("Nombres");
                String CC = result.getString("CC");
                factura = new ClsFactura(Id, Fecha, idClient, Descuento, Total, Cliente, CC);
                factura_list.add(factura);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
        return factura_list;
    }
}
