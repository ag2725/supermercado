/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author ever
 */
public class DbData {
    private final String driver = "com.mysql.cj.jdbc.Driver";// "org.sqlite.JDBC";//
    private final String user = "root";
    private final String password = "123";
    private final String url = "jdbc:mysql://localhost:3306/supermercado?zeroDateTimeBehavior=CONVERT_TO_NULL";// "jdbc:sqlite:order.sqlite3";//

    public String getDriver() {
        return driver;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getUrl() {
        return url;
    }
    
}
