/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import classes.ClsClientes;
import classes.ClsTelefonosClientes;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import static java.sql.Statement.RETURN_GENERATED_KEYS;
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author ever
 */
public class ModelClientes {
    DbData dbData;
    ClsClientes cliente;
    public ModelClientes() {
        this.dbData = new DbData();
        try {
            Class.forName(dbData.getDriver());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    public ClsClientes Create(ClsClientes cliente){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "INSERT INTO tb_clientes(CC, Nombres, Apellidos, Direccion, CorreoElectronico)\n" +
                            "VALUES(?, ?, ?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(query, RETURN_GENERATED_KEYS);
            statement.setString(1, cliente.getCC());
            statement.setString(2, cliente.getNombres());
            statement.setString(3, cliente.getApellidos());
            statement.setString(4, cliente.getDireccion());
            statement.setString(5, cliente.getCorreoElectronico());
            int rowInsert = statement.executeUpdate();
            int idCliente=0;
            if(rowInsert>0){
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if(generatedKeys.next()){
                    idCliente = generatedKeys.getInt(1);
                }
            }
            return Search(idCliente);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
    public ClsClientes Edit(ClsClientes cliente){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "UPDATE tb_clientes\n" +
                            "SET CC=?, Nombres=?, Apellidos=?, Direccion=?, CorreoElectronico=? WHERE Id=?;";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, cliente.getCC());
            statement.setString(2, cliente.getNombres());
            statement.setString(3, cliente.getApellidos());
            statement.setString(4, cliente.getDireccion());
            statement.setString(5, cliente.getCorreoElectronico());
            statement.setInt(6, cliente.getId());
            statement.executeUpdate();
            return Search(cliente.getId());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
    public boolean Delete(ClsClientes cliente){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "DELETE FROM tb_clientes WHERE Id=?";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(1, cliente.getId());
            int rowDelete = statement.executeUpdate();
            if(rowDelete>0){
               return true; 
            }else{
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
    public ClsClientes Search(Integer idCategoria){
        cliente = null;
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "SELECT * FROM tb_clientes WHERE Id=?;";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(1, idCategoria);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                Integer Id = result.getInt("Id");
                String CC = result.getString("CC");
                String Nombres = result.getString("Nombres");
                String Apellidos = result.getString("Apellidos");
                String Direccion = result.getString("Direccion");
                String CorreoElectronico = result.getString("CorreoElectronico");
                cliente = new ClsClientes(Id, CC, Nombres, Apellidos, Direccion, CorreoElectronico);
            }
            query = "SELECT * FROM tb_telefonos_clientes WHERE idCliente=?;";
            statement = conn.prepareStatement(query);
            statement.setInt(1, cliente.getId());
            result = statement.executeQuery();
            while (result.next()){
                Integer Id = result.getInt("Id");
                Integer idCliente = result.getInt("idCliente");
                String telefono = result.getString("telefono");
                ClsTelefonosClientes telefonoActual = new ClsTelefonosClientes(Id, idCliente, telefono);
                cliente.setTelefono(telefonoActual);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
        return cliente;
    }
     
    public LinkedList<ClsClientes> All(){
        LinkedList<ClsClientes> cliente_list = new LinkedList<>();
        cliente = null;
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "SELECT * FROM tb_clientes;";
            PreparedStatement statement = conn.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                Integer Id = result.getInt("Id");
                String CC = result.getString("CC");
                String Nombres = result.getString("Nombres");
                String Apellidos = result.getString("Apellidos");
                String Direccion = result.getString("Direccion");
                String CorreoElectronico = result.getString("CorreoElectronico");
                cliente = new ClsClientes(Id, CC, Nombres, Apellidos, Direccion, CorreoElectronico);
                cliente_list.add(cliente);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
        return cliente_list;
    }
}
