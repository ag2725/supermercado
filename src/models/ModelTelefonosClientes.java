/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import classes.ClsTelefonosClientes;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import static java.sql.Statement.RETURN_GENERATED_KEYS;
import javax.swing.JOptionPane;

/**
 *
 * @author ever
 */
public class ModelTelefonosClientes {
    DbData dbData;
    ClsTelefonosClientes telefono;
    public ModelTelefonosClientes() {
        this.dbData = new DbData();
        try {
            Class.forName(dbData.getDriver());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    public boolean Create(ClsTelefonosClientes telefono){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "INSERT INTO tb_telefonos_clientes(IdCliente, telefono) VALUES(?, ?)";
            PreparedStatement statement = conn.prepareStatement(query, RETURN_GENERATED_KEYS);
            statement.setInt(1, telefono.getIdCliente());
            statement.setString(2, telefono.getTelefono());
            int rowInsert = statement.executeUpdate();
            int idCliente=0;
            if(rowInsert>0){
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if(generatedKeys.next()){
                    idCliente = generatedKeys.getInt(1);
                }
            }
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }
    }
    public boolean Edit(ClsTelefonosClientes telefono){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "UPDATE tb_telefonos_clientes\n" +
                            "SET IdCliente=?, telefono=? WHERE Id=?;";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(1, telefono.getIdCliente());
            statement.setString(2, telefono.getTelefono());
            statement.setInt(3, telefono.getId());
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return false;
        }
    }
    public boolean Delete(ClsTelefonosClientes telefono){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "DELETE FROM tb_telefonos_clientes WHERE Id=?";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(1, telefono.getId());
            int rowDelete = statement.executeUpdate();
            if(rowDelete>0){
               return true; 
            }else{
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
}
