/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import classes.ClsProductos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import static java.sql.Statement.RETURN_GENERATED_KEYS;
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author ever
 */
public class ModelProducto {
    DbData dbData;
    ClsProductos producto;
    public ModelProducto() {
        this.dbData = new DbData();
        try {
            Class.forName(dbData.getDriver());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    public ClsProductos Create(ClsProductos producto){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "INSERT INTO tb_productos(Nombre, PrecioActual, Existencia, idCategoria, idProveedor)\n" +
                            "VALUES(?, ?, ?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(query, RETURN_GENERATED_KEYS);
            statement.setString(1, producto.getNombre());
            statement.setFloat(2, producto.getPrecioActual());
            statement.setInt(3, producto.getExistencia());
            statement.setInt(4, producto.getIdCategoria());
            statement.setInt(5, producto.getIdProveedor());
            int rowInsert = statement.executeUpdate();
            int idProducto=0;
            if(rowInsert>0){
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if(generatedKeys.next()){
                    idProducto = generatedKeys.getInt(1);
                }
            }
            return Search(idProducto);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
    public ClsProductos Edit(ClsProductos producto){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "UPDATE tb_productos\n" +
                            "SET Nombre=?, PrecioActual=?, Existencia=?, idCategoria=?, idProveedor=? WHERE Id=?;";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, producto.getNombre());
            statement.setFloat(2, producto.getPrecioActual());
            statement.setInt(3, producto.getExistencia());
            statement.setInt(4, producto.getIdCategoria());
            statement.setInt(5, producto.getIdProveedor());
            statement.setInt(6, producto.getId());
            statement.executeUpdate();
            return Search(producto.getId());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
    public boolean Delete(ClsProductos producto){
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "DELETE FROM tb_productos WHERE Id=?";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(1, producto.getId());
            int rowDelete = statement.executeUpdate();
            if(rowDelete>0){
               return true; 
            }else{
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
    public ClsProductos Search(Integer idProducto){
        producto = null;
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "SELECT * FROM tb_productos WHERE Id=?;";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(1, idProducto);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                Integer Id = result.getInt("Id");
                String Nombre = result.getString("Nombre");
                Float PrecioActual = result.getFloat("PrecioActual");
                Integer Existencia = result.getInt("Existencia");
                Integer idCategoria = result.getInt("idCategoria");
                Integer idProveedor = result.getInt("idProveedor");
                producto = new ClsProductos(Id, Nombre, PrecioActual, Existencia, idCategoria, idProveedor);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
        return producto;
    }
     
    public LinkedList<ClsProductos> All(){
        LinkedList<ClsProductos> producto_list = new LinkedList<>();
        producto = null;
        try (Connection conn = DriverManager.getConnection(dbData.getUrl(), dbData.getUser(), dbData.getPassword())) {
            String query = "SELECT pro.id, pro.Nombre, pro.PrecioActual, pro.Existencia, pro.idCategoria, pro.idProveedor, cat.Nombre,\n" +
                "(SELECT SUM(prof.Cantidad) FROM tb_productos_to_facturas AS prof WHERE pro.Id=prof.idProducto) AS vendidos,\n" +
                "cat.Nombre AS categoria, prov.RazonSocial AS proveedor\n" +
                "FROM tb_productos AS pro \n" +
                "INNER JOIN tb_categoria AS cat\n" +
                "ON pro.idCategoria=cat.Id\n" +
                "INNER JOIN tb_proveedores AS prov\n" +
                "ON pro.idProveedor=prov.Id\n" +
                "GROUP BY pro.Id;";
            PreparedStatement statement = conn.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                Integer Id = result.getInt("Id");
                String Nombre = result.getString("Nombre");
                Float PrecioActual = result.getFloat("PrecioActual");
                Integer Existencia = result.getInt("Existencia");
                Integer idCategoria = result.getInt("idCategoria");
                Integer idProveedor = result.getInt("idProveedor");
                String categoria = result.getString("categoria");
                String proveedor = result.getString("proveedor");
                Integer vendidos = result.getInt("vendidos");
                producto = new ClsProductos(
                    Id, Nombre, PrecioActual, Existencia, idCategoria,
                    idProveedor, categoria, proveedor, vendidos
                );
                producto_list.add(producto);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
        return producto_list;
    }
}
