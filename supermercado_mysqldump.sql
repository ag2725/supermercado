-- MySQL dump 10.13  Distrib 5.7.35, for Linux (x86_64)
--
-- Host: localhost    Database: supermercado
-- ------------------------------------------------------
-- Server version	5.7.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_categoria`
--

DROP TABLE IF EXISTS `tb_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_categoria` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(70) NOT NULL,
  `Descripcion` varchar(250) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_categoria`
--

LOCK TABLES `tb_categoria` WRITE;
/*!40000 ALTER TABLE `tb_categoria` DISABLE KEYS */;
INSERT INTO `tb_categoria` VALUES (1,'Frutas y verduras','Vegetales, pulpas y frutas'),(2,'Carnes','Carnicos, pescados y mariscos'),(3,'Despensa','Granos secos, aceites, panela, endulsantes, etc'),(4,'Panaderia','Levaduras, arinas e ingredientes en general'),(5,'Lacteos, huevos y refrigerados','Arepas, tortillas, embutidos, huevos y lacteos'),(6,'Bebidas y snacks','Pasabocas, chocolates, confiteria, dulces, galletas y bebidas'),(7,'Licores','Cerveza, vino, aguardiente, whisky, etc');
/*!40000 ALTER TABLE `tb_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_clientes`
--

DROP TABLE IF EXISTS `tb_clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_clientes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CC` varchar(20) NOT NULL,
  `Nombres` varchar(20) NOT NULL,
  `Apellidos` varchar(20) NOT NULL,
  `Direccion` varchar(150) NOT NULL,
  `CorreoElectronico` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_clientes`
--

LOCK TABLES `tb_clientes` WRITE;
/*!40000 ALTER TABLE `tb_clientes` DISABLE KEYS */;
INSERT INTO `tb_clientes` VALUES (1,'1043709540','NANCY ASTRID','BARON LOPEZ','Calle Falsa 123 Laketow Springfield','baron@gmail.com'),(2,'1057763322','ANGEL IGNACIO','BECERRA VALDERRAMA','Carrera 45 12-32 Colon Medellin','becerra@gmail.com'),(3,'1066762341','ADRIANA CECILIA','BENITEZ CUBAQUE','Calle Juan Delgado 12-23 Tauma La Habana','benitez@gmail.com'),(4,'1054761333','JOSE LUIS','CAMPO OTALORA','Avenida 7A 01004 Centro Guatemala','campo@gmail.com'),(5,'1063760369','FREDY ALONSO','CANON MARTINEZ','Calle Falsa 123 Laketow Springfield','canon@gmail.com');
/*!40000 ALTER TABLE `tb_clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_direcciones`
--

DROP TABLE IF EXISTS `tb_direcciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_direcciones` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CalleCarrera` varchar(20) NOT NULL,
  `Numero` varchar(20) NOT NULL,
  `Barrio` varchar(20) NOT NULL,
  `Ciudad` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_direcciones`
--

LOCK TABLES `tb_direcciones` WRITE;
/*!40000 ALTER TABLE `tb_direcciones` DISABLE KEYS */;
INSERT INTO `tb_direcciones` VALUES (1,'Calle Falsa','123','Laketow','Springfield'),(2,'Carrera 45','12-32','Colon','Medellin'),(3,'Calle Juan Delgado','12-23','Tauma','La Habana'),(4,'Avenida 7A','01004','Centro','Guatemala');
/*!40000 ALTER TABLE `tb_direcciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_facturas`
--

DROP TABLE IF EXISTS `tb_facturas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_facturas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idClient` int(11) NOT NULL,
  `Descuento` float NOT NULL,
  `Total` float NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `idClient` (`idClient`),
  CONSTRAINT `tb_facturas_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `tb_clientes` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_facturas`
--

LOCK TABLES `tb_facturas` WRITE;
/*!40000 ALTER TABLE `tb_facturas` DISABLE KEYS */;
INSERT INTO `tb_facturas` VALUES (1,'2021-08-15 15:40:48',3,0,680680),(2,'2021-08-15 15:40:48',4,0,3545840),(3,'2021-08-15 15:40:48',2,0,4987410);
/*!40000 ALTER TABLE `tb_facturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_productos`
--

DROP TABLE IF EXISTS `tb_productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_productos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(70) NOT NULL,
  `PrecioActual` float NOT NULL,
  `Existencia` int(11) NOT NULL,
  `idCategoria` int(11) NOT NULL,
  `idProveedor` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `idCategoria` (`idCategoria`),
  KEY `idProveedor` (`idProveedor`),
  CONSTRAINT `tb_productos_ibfk_1` FOREIGN KEY (`idCategoria`) REFERENCES `tb_categoria` (`Id`),
  CONSTRAINT `tb_productos_ibfk_2` FOREIGN KEY (`idProveedor`) REFERENCES `tb_proveedores` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_productos`
--

LOCK TABLES `tb_productos` WRITE;
/*!40000 ALTER TABLE `tb_productos` DISABLE KEYS */;
INSERT INTO `tb_productos` VALUES (1,'Lechuga',1500,19,1,4),(2,'Berenjena',1350,12,1,4),(3,'Guatilla',850,12,1,4),(4,'Alberja Verde Lb',3500,15,1,4),(5,'Mazorca Bandeja',3350,30,1,4),(6,'Lomo de res Lb',9000,50,2,3),(7,'Lomo de cerdo Lb',11000,47,2,3),(8,'Trucha Lb',9000,45,2,3),(9,'Calamar Lb',35000,65,2,3),(10,'Robalo Lb',15000,70,2,3),(11,'Arroz Lb',1800,300,3,2),(12,'Fijol Lb',3500,150,3,2),(13,'Aceite Lt',8000,133,3,2),(14,'Panela',2000,876,3,2),(15,'Lenteja Lb',2500,500,3,2),(16,'Cerveza X30',55000,100,7,1),(17,'Cerveza X6',11500,300,7,1),(18,'Cerveza',2000,2000,7,1),(19,'Aguardiente Lt X10',350000,1000,7,1),(20,'Whiskey Botella',70000,1230,7,1);
/*!40000 ALTER TABLE `tb_productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_productos_to_facturas`
--

DROP TABLE IF EXISTS `tb_productos_to_facturas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_productos_to_facturas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PrecioUnitario` float NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `idFactura` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `idFactura` (`idFactura`),
  KEY `idProducto` (`idProducto`),
  CONSTRAINT `tb_productos_to_facturas_ibfk_1` FOREIGN KEY (`idFactura`) REFERENCES `tb_facturas` (`Id`),
  CONSTRAINT `tb_productos_to_facturas_ibfk_2` FOREIGN KEY (`idProducto`) REFERENCES `tb_productos` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_productos_to_facturas`
--

LOCK TABLES `tb_productos_to_facturas` WRITE;
/*!40000 ALTER TABLE `tb_productos_to_facturas` DISABLE KEYS */;
INSERT INTO `tb_productos_to_facturas` VALUES (1,0,12,1,1),(2,0,45,1,2),(3,0,32,1,3),(4,0,52,1,4),(5,0,23,1,5),(6,0,23,1,6),(7,0,56,2,7),(8,0,45,2,8),(9,0,34,2,9),(10,0,23,2,10),(11,0,44,2,11),(12,0,23,2,12),(13,0,33,2,13),(14,0,12,3,11),(15,0,12,3,12),(16,0,54,3,13),(17,0,34,3,14),(18,0,34,3,15),(19,0,55,3,16),(20,0,45,3,17);
/*!40000 ALTER TABLE `tb_productos_to_facturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_proveedores`
--

DROP TABLE IF EXISTS `tb_proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_proveedores` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nit` varchar(20) NOT NULL,
  `RazonSocial` varchar(70) NOT NULL,
  `Telefono` varchar(20) NOT NULL,
  `CorreoElectronico` varchar(70) NOT NULL,
  `RepresentanteLegal` varchar(20) NOT NULL,
  `SitioWeb` varchar(70) NOT NULL,
  `idDireccion` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `idDireccion` (`idDireccion`),
  CONSTRAINT `tb_proveedores_ibfk_1` FOREIGN KEY (`idDireccion`) REFERENCES `tb_direcciones` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_proveedores`
--

LOCK TABLES `tb_proveedores` WRITE;
/*!40000 ALTER TABLE `tb_proveedores` DISABLE KEYS */;
INSERT INTO `tb_proveedores` VALUES (1,'809004555-2','SURTITIENDAS S.A.S','3103598773','gerencia@surtitiendas.com','Don Jeronimo','http://surtitiendas.com/',1),(2,'890900608-9','CARULLA S.A.S','3104598733','gerencia@carulla.com','Don Ramon','https://www.carulla.com/',2),(3,'890900608-9','EXITO S.A.S','3104598373','gerencia@exito.com','Sarmiento Angulo','https://www.exito.com/',3),(4,'809007555-1','SURTIMAX S.A.S','3104398773','gerencia@surtimax.com','Juan Carlos Bodoque','https://www.surtimax.com.co/',4);
/*!40000 ALTER TABLE `tb_proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_telefonos_clientes`
--

DROP TABLE IF EXISTS `tb_telefonos_clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_telefonos_clientes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `idCliente` int(11) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `idCliente` (`idCliente`),
  CONSTRAINT `tb_telefonos_clientes_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `tb_clientes` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_telefonos_clientes`
--

LOCK TABLES `tb_telefonos_clientes` WRITE;
/*!40000 ALTER TABLE `tb_telefonos_clientes` DISABLE KEYS */;
INSERT INTO `tb_telefonos_clientes` VALUES (1,1,'3278339621'),(2,1,'3127339213'),(3,2,'3108332193'),(4,3,'3108321693'),(5,3,'3127219693'),(6,3,'3102179693'),(7,4,'3221336693'),(8,4,'3218327693'),(9,4,'3221339693'),(10,5,'3102139273');
/*!40000 ALTER TABLE `tb_telefonos_clientes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-15 10:46:48
