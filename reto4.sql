/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  ever
 * Created: 10 ago. 2021
 */


CREATE TABLE tb_direcciones(
    Id INT AUTO_INCREMENT PRIMARY KEY,
    CalleCarrera VARCHAR(20) NOT NULL,
    Numero VARCHAR(20) NOT NULL,
    Barrio VARCHAR(20) NOT NULL,
    Ciudad VARCHAR(20) NOT NULL
);

CREATE TABLE tb_proveedores(
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Nit VARCHAR(20) NOT NULL,
    RazonSocial VARCHAR(70) NOT NULL,
    Telefono VARCHAR(20) NOT NULL,
    CorreoElectronico VARCHAR(70) NOT NULL,
    RepresentanteLegal VARCHAR(20) NOT NULL,
    SitioWeb VARCHAR(70) NOT NULL,
    idDireccion INT NOT NULL,
    FOREIGN KEY(idDireccion) REFERENCES tb_direcciones(Id)
);

CREATE TABLE tb_clientes(
    Id INT AUTO_INCREMENT PRIMARY KEY,
    CC VARCHAR(20) NOT NULL,
    Nombres VARCHAR(20) NOT NULL,
    Apellidos VARCHAR(20) NOT NULL,
    Direccion VARCHAR(150) NOT NULL,
    CorreoElectronico VARCHAR(20) NOT NULL
);

CREATE TABLE tb_telefonos_clientes(
    Id INT AUTO_INCREMENT PRIMARY KEY,
    idCliente INT NOT NULL,
    telefono VARCHAR(20) NOT NULL,
    FOREIGN KEY(idCliente) REFERENCES tb_clientes(Id)
);

CREATE TABLE tb_categoria(
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Nombre VARCHAR(70) NOT NULL,
    Descripcion VARCHAR(250) NOT NULL
);

CREATE TABLE tb_productos(
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Nombre VARCHAR(70) NOT NULL,
    PrecioActual FLOAT NOT NULL,
    Existencia INT NOT NULL,
    idCategoria INT NOT NULL,
    idProveedor INT NOT NULL,
    FOREIGN KEY(idCategoria) REFERENCES tb_categoria(Id),
    FOREIGN KEY(idProveedor) REFERENCES tb_proveedores(Id)
);

CREATE TABLE tb_facturas(
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    idClient INT NOT NULL,
    Descuento FLOAT NOT NULL,
    Total FLOAT NOT NULL,
    FOREIGN KEY(idClient) REFERENCES tb_clientes(Id)
);

CREATE TABLE tb_productos_to_facturas(
    Id INT AUTO_INCREMENT PRIMARY KEY,
    PrecioUnitario FLOAT NOT NULL,
    Cantidad INT NOT NULL,
    idFactura INT NOT NULL,
    idProducto INT NOT NULL,
    FOREIGN KEY(idFactura) REFERENCES tb_facturas(Id),
    FOREIGN KEY(idProducto) REFERENCES tb_productos(Id)
);

INSERT INTO tb_categoria(Nombre, Descripcion)
VALUES
    ("Frutas y verduras","Vegetales, pulpas y frutas"),
    ("Carnes","Carnicos, pescados y mariscos"),
    ("Despensa","Granos secos, aceites, panela, endulsantes, etc"),
    ("Panaderia","Levaduras, arinas e ingredientes en general"),
    ("Lacteos, huevos y refrigerados","Arepas, tortillas, embutidos, huevos y lacteos"),
    ("Bebidas y snacks","Pasabocas, chocolates, confiteria, dulces, galletas y bebidas"),
    ("Licores","Cerveza, vino, aguardiente, whisky, etc");

INSERT INTO tb_direcciones(CalleCarrera, Numero, Barrio, Ciudad)
VALUES
    ("Calle Falsa", "123", "Laketow", "Springfield"),
    ("Carrera 45", "12-32", "Colon", "Medellin"),
    ("Calle Juan Delgado", "12-23", "Tauma", "La Habana"),
    ("Avenida 7A", "01004", "Centro", "Guatemala");

INSERT INTO tb_clientes(CC, Nombres, Apellidos, Direccion, CorreoElectronico)
VALUES
    ("1043709540", "NANCY ASTRID", "BARON LOPEZ", "Calle Falsa 123 Laketow Springfield", "baron@gmail.com"),
    ("1057763322", "ANGEL IGNACIO", "BECERRA VALDERRAMA", "Carrera 45 12-32 Colon Medellin", "becerra@gmail.com"),
    ("1066762341", "ADRIANA CECILIA", "BENITEZ CUBAQUE", "Calle Juan Delgado 12-23 Tauma La Habana", "benitez@gmail.com"),
    ("1054761333", "JOSE LUIS", "CAMPO OTALORA", "Avenida 7A 01004 Centro Guatemala", "campo@gmail.com"),
    ("1063760369", "FREDY ALONSO", "CANON MARTINEZ", "Calle Falsa 123 Laketow Springfield", "canon@gmail.com");

INSERT INTO tb_telefonos_clientes(idCliente, telefono)
VALUES
    (1, "3278339621"),
    (1, "3127339213"),
    (2, "3108332193"),
    (3, "3108321693"),
    (3, "3127219693"),
    (3, "3102179693"),
    (4, "3221336693"),
    (4, "3218327693"),
    (4, "3221339693"),
    (5, "3102139273");

INSERT INTO tb_proveedores(Nit, RazonSocial, Telefono, CorreoElectronico, RepresentanteLegal, SitioWeb, idDireccion)
VALUES
    ("809004555-2", "SURTITIENDAS S.A.S", "3103598773", "gerencia@surtitiendas.com", "Don Jeronimo", "http://surtitiendas.com/", 1),
    ("890900608-9", "CARULLA S.A.S", "3104598733", "gerencia@carulla.com", "Don Ramon", "https://www.carulla.com/", 2),
    ("890900608-9", "EXITO S.A.S", "3104598373", "gerencia@exito.com", "Sarmiento Angulo", "https://www.exito.com/", 3),
    ("809007555-1", "SURTIMAX S.A.S", "3104398773", "gerencia@surtimax.com", "Juan Carlos Bodoque", "https://www.surtimax.com.co/", 4);

INSERT INTO tb_productos(Nombre, PrecioActual, Existencia, idCategoria, idProveedor)
VALUES
    ("Lechuga", 1500, 19, 1, 4),
    ("Berenjena", 1350, 12, 1, 4),
    ("Guatilla", 850, 12, 1, 4),
    ("Alberja Verde Lb", 3500, 15, 1, 4),
    ("Mazorca Bandeja", 3350, 30, 1, 4),

    ("Lomo de res Lb", 9000, 50, 2, 3),
    ("Lomo de cerdo Lb", 11000, 47, 2, 3),
    ("Trucha Lb", 9000, 45, 2, 3),
    ("Calamar Lb", 35000, 65, 2, 3),
    ("Robalo Lb", 15000, 70, 2, 3),

    ("Arroz Lb", 1800, 300, 3, 2),
    ("Fijol Lb", 3500, 150, 3, 2),
    ("Aceite Lt", 8000, 133, 3, 2),
    ("Panela", 2000, 876, 3, 2),
    ("Lenteja Lb", 2500, 500, 3, 2),

    ("Cerveza X30", 55000, 100, 7, 1),
    ("Cerveza X6", 11500, 300, 7, 1),
    ("Cerveza", 2000, 2000, 7, 1),
    ("Aguardiente Lt X10", 350000, 1000, 7, 1),
    ("Whiskey Botella", 70000, 1230, 7, 1);

INSERT INTO tb_facturas(idClient, Descuento, Total)
VALUES
    (3, 0, 680680),
    (4, 0, 3545843),
    (2, 0, 4987409);

INSERT INTO tb_productos_to_facturas(PrecioUnitario, Cantidad, idFactura, idProducto)
VALUES
    (0, 12, 1, 1),
    (0, 45, 1, 2),
    (0, 32, 1, 3),
    (0, 52, 1, 4),
    (0, 23, 1, 5),
    (0, 23, 1, 6),

    (0, 56, 2, 7),
    (0, 45, 2, 8),
    (0, 34, 2, 9),
    (0, 23, 2, 10),
    (0, 44, 2, 11),
    (0, 23, 2, 12),
    (0, 33, 2, 13),

    (0, 12, 3, 11),
    (0, 12, 3, 12),
    (0, 54, 3, 13),
    (0, 34, 3, 14),
    (0, 34, 3, 15),
    (0, 55, 3, 16),
    (0, 45, 3, 17);



SELECT * FROM tb_proveedores LIMIT 100;

SELECT * FROM tb_productos LIMIT 100;

SELECT * FROM tb_facturas LIMIT 100;

UPDATE tb_proveedores SET RazonSocial="SURTITIENDAS S.A" WHERE Id=1;

UPDATE tb_productos SET idCategoria=6 WHERE Nombre LIKE "Cerv%";

/*Aplica a todos los campos*/
UPDATE tb_facturas SET Descuento=500;

DELETE FROM tb_productos_to_facturas WHERE idProducto=20;
DELETE FROM tb_productos WHERE Nombre="Whiskey Botella";


SELECT * FROM tb_facturas as fac, tb_productos as pro ORDER BY fac.id;

SELECT fac.Id, pro.Nombre, pro.PrecioActual, profac.Cantidad, profac.Subtotal,
    cli.Nombres, cli.Apellidos, cli.Direccion 
    FROM
    tb_facturas as fac 
    INNER JOIN tb_productos_to_facturas as profac ON fac.Id=profac.idFactura 
    INNER JOIN tb_productos as pro ON profac.idProducto=pro.Id
    INNER JOIN tb_clientes as cli ON fac.idClient=cli.Idz
    ORDER BY fac.id;

SELECT pro.Id, pro.Nit, pro.RazonSocial, pro.Telefono, pro.CorreoElectronico,
    pro.RepresentanteLegal, pro.SitioWeb, pro.idDireccion,
    dir.CalleCarrera, dir.Numero, dir.Barrio, dir.Ciudad FROM tb_proveedores AS pro
    INNER JOIN tb_direcciones AS dir ON pro.idDireccion=dir.Id WHERE pro.Id=1;
/*
UPDATE tb_proveedores AS pro INNER JOIN tb_direcciones AS dir
ON pro.idDireccion=dir.Id
SET pro.Nit=?, pro.RazonSocial=?, pro.Telefono=?, pro.CorreoElectronico=?,
pro.RepresentanteLegal=?, pro.SitioWeb=?, pro.idDireccion=?,
dir.CalleCarrera=?, dir.Numero=?, dir.Barrio=?, dir.Ciudad=?
WHERE pro.Id=1;

INSERT INTO tb_proveedores(Nit, RazonSocial, Telefono, CorreoElectronico, RepresentanteLegal, SitioWeb, idDireccion)
VALUES("wef", "sadasd", "wadfasd", "sfdgvsd", "dfv", "fdvgdf", 6)

*/

SELECT cat.Id, cat.Nombre, cat.Descripcion,
COUNT(*) AS producto
FROM tb_categoria AS cat INNER JOIN tb_productos AS pro
ON cat.Id=pro.idCategoria GROUP BY cat.Id;

SELECT cat.Id, cat.Nombre, cat.Descripcion,
(SELECT COUNT(*) FROM tb_productos AS pro WHERE pro.idCategoria=cat.Id) AS productos
FROM tb_categoria AS cat WHERE Id=1;

SELECT pro.Nombre FROM tb_productos AS pro INNER JOIN tb_categoria AS cat GROUP BY cat.Id ON cat.Id=pro.idCategoria;

SELECT pro.id, pro.Nombre, pro.PrecioActual, pro.Existencia, pro.idCategoria, pro.idProveedor, cat.Nombre,
(SELECT SUM(prof.Cantidad) FROM tb_productos_to_facturas AS prof WHERE pro.Id=prof.idProducto) AS vendidos,
cat.Nombre AS categoria, prov.RazonSocial AS proveedor
FROM tb_productos AS pro 
INNER JOIN tb_categoria AS cat
ON pro.idCategoria=cat.Id
INNER JOIN tb_proveedores AS prov
ON pro.idProveedor=prov.Id
GROUP BY pro.Id;


SELECT Cantidad FROM tb_productos_to_facturas WHERE idProducto=1;

SELECT prof.Id, prof.PrecioUnitario, prof.Cantidad, prof.idProducto, pro.Nombre, pro.Existencia
FROM tb_productos_to_facturas AS prof
INNER JOIN tb_productos AS pro
ON prof.idProducto=pro.Id
WHERE prof.idFactura=1
GROUP BY prof.Id;

SELECT fac.Id, fac.Fecha, fac.idClient, fac.Descuento, fac.Total, cli.Nombres, cli.Apellidos, cli.CC
FROM tb_facturas AS fac
INNER JOIN tb_clientes AS cli
ON fac.idClient=cli.Id
GROUP BY fac.Id;

SELECT fac.Id, fac.Fecha, fac.idClient, fac.Descuento, fac.Total, cli.Nombres, cli.Apellidos, cli.CC
FROM tb_facturas AS fac
INNER JOIN tb_clientes AS cli
ON fac.idClient=cli.Id
WHERE fac.Id=1
GROUP BY fac.Id;

INSERT INTO tb_productos_to_facturas(PrecioUnitario, Cantidad, idFactura, idProducto)

VALUES(?, ?, ?, ?)